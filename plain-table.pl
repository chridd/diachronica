#! /usr/bin/perl -Ihtml/
use utf8; use strict; use warnings;
use Diachronica;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

my $table = {is_plain=>1};

read_data(
	change => sub {
		my %c = parse_sound_change($_[0]);
		add_sounds_to_table($table, sounds(%c));
	}
);
make_sounds_table($table);

