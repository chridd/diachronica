#! /usr/bin/perl -Ihtml/
use Diachronica;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

my $fh = open_data;
my @sounds;

while(<$fh>) {
	if(/^C (.*)$/) {
		my %change = parse_sound_change($1);
		for my $c (@{$change{changes}}) {
			push @sounds, $c->{from}, $c->{to};
		}
		$c->{context} =~ /(.*)_(.*)/ and push @sounds, $1, $2;
	}
}

@sounds = map {s{\[[^\]]+\]}{}gr} @sounds;
@sounds = map {s{</?su[pb]>}{}gr} @sounds;
@sounds = sort {length $a <=> length $b || $a cmp $b} @sounds;
print join("\n", @sounds);
print "\n";

#my ($changes, $tables) = find_sound_changes;
#my @sounds;
#
#for my $c (@{$changes}) {
#	for my $s (@{$c->{start}}, @{$c->{end}}, @{$c->{context}}) {
#		push @sounds, $s;
#	}
#}
#
#@sounds = sort {length $a <=> length $b || $a cmp $b} @sounds;
#print join("\n", @sounds);
#print "\n";
