#! /usr/bin/perl
use utf8;
die "Not a CGI script" if $ENV{SERVER_NAME};

my $in;
open $in, '<', 'html/index-diachronica.tex' or die "Can't open input file $!";

open STDOUT, '>:utf8', 'data-raw.txt' or die "Can't open output file $!";
binmode STDOUT, ':utf8';

$_ = <$in> until /\\pagenumbering\{arabic\}/;
my $warnings = 0;
$SIG{__WARN__} = sub { warn $_[0]; $warnings++; };

my %TEXT_CMDS = (
	textbackslash => "\\",
	textellipsis => '…',
	change => '→',
	textrightarrow => '→',
	textleftarrow => '←',
	LaTeX => 'LaTeX',
#	tab => '<p>',
	tab => '',
	item => '<li>',
	clearpage => '',
	langle => '〈',
	rangle => '〉',
	textless => '&lt;',
	textgreater => '&gt;',
	textquoteleft => "‘",
	textquotedblleft => '“',
	hline => '',
	textasciitilde => '~',
	textturnw => 'ʍ',
	aa => 'å',
	AA => 'Å',
	Omega => 'Ω',
	textsoftsign => 'ь',
	texthardsign => 'ъ',
	l => 'ł',
	textcorner => "\x{31a}", #'⌝',
	i => 'ı',
	j => 'j', # dotless with accent above
	L => 'Ł',
	
	# IPA - http://www.ling.ohio-state.edu/events/lcc/tutorials/tipachart/tipachart.pdf
	# \c{c} = ç  || = ǁ  "" = ˌ
	# TODO tone?
	textbeltl => 'ɬ',
	textlyoghlig => 'ɮ',
	textbardotlessj => 'ɟ',
	textltailn => 'ɲ',
	textturnmrleg => 'ɰ',
	textcrh => 'ħ',
	texththeng => 'ɧ',
	textturnlonglegr => 'ɺ',
	textctz => 'ʑ',
	textctn => 'ȵ',
	textdoublebarpipe => 'ǂ',
	textbarglotstop => 'ʡ',
	textbarrevglotstop => 'ʢ',
	textcloseepsilon => 'ɞ',
	textturna => 'ɐ',
	textlhtlongi => 'ɿ', # except longer? is this even in Unicode?
	textraisevibyi => 'ʅ',
	O => '∅',
	o => 'ø',
	oe => 'œ',
	ae => 'æ',
	OE => 'ɶ',
	'!o', 'ʘ',
	'!b', 'ɓ',
	'!d', 'ɗ',
	'!j', 'ʄ',
	'!g', 'ɠ',
	'!G', 'ʛ',
	';B' => 'ʙ',
	';L' => 'ʟ',
	';G', 'ɢ',
	';N', 'ɴ',
	';R', 'ʀ',
	';H', 'ʜ',
	'*r' => 'ɹ',
	'*w', 'ʍ',
	':t' => 'ʈ',
	':d' => 'ɖ',
	':n', 'ɳ',
	':r', 'ɽ',
	':s', 'ʂ',
	':z', 'ʐ',
	':R', 'ɻ',
	':l', 'ɭ',
	':g', 'ɡ', #?
	':G', 'ɣ', #?
	';X', 'χ', #?
#	'|`', "\x{31e}",
#	'|[', "\x{32a}",
#	'|\'', "\x{31d}",
#	'|]', "\x{33a}",
);
my %DIACRITICS = (qw(
	` ̀
	' ́
	^ ̂
	" ̈
	H ̋
	~ ̃
	c ̧
	k ̨
	= ̄
	. ̇
	d ̣
	r ̊
	u ̆
	v ̌
	t ͡
	s ̩
	=* ̲
	r* ̥
	v* ̬
	t* ͜
	~* ̰
	|` ̞
	|[ ̪
	|' ̝
	|] ̺
	|m ̼
	textsubarch ̯
	textsubsquare ̻
	textsubring ̥
	textsuperimposetilde ̴
	textpolhook ̨
	cc ç
	ka ą
	ke ę
	ki į
	ko ǫ
	ku ų
	textpolhooka ą
	textpolhooke ę
	textpolhooki į
	textpolhooko ǫ
	textpolhooku ų
	textsuperimposetildel ɫ
));
my %SUPERS = (qw(
	1 ¹ 2 ² 3 ³ 4 ⁴ 5 ⁵ 6 ⁶ 7 ⁷ 8 ⁸ 9 ⁹ 0 ⁰
	a ᵃ b ᵇ d ᵈ e ᵉ g ᵍ h ʰ i ⁱ j ʲ k ᵏ l ˡ m ᵐ n ⁿ o ᵒ p ᵖ r ʳ t ᵗ u ᵘ v ᵛ w ʷ x ˟ y ʸ z ᶻ
	Q ˤ P ˀ N ᵑ H ʱ W ᵚ M ᶬ
));

sub hlchar {
	my ($line, $pos) = @_;
	substr($line, $pos, 1) = "\e[41m".substr($line, $pos, 1)."\e[0m";
	return $line;
}

my $in_table = 0;
my $table = '';
my $want_break = 0;
my @ends;
sub parseLaTeX {
	local $_ = $_[0];
	my $r = '';
	$r .= "\x{e002}" if $in_table;
	my $ipamode = 0;
	my $prevpos = 0;
	s{@\\textrhoticity\s*}{ɚ}g;
	if($want_break) {
		$r .= $want_break;
		$want_break = 0;
	}
	while(1) {
	#	print STDERR pos, "," if $. == 5771;
		if(/\G$/gc) {
			last;
		} elsif(/\G\{\\([A-Za-z]{2})\b\s*/gc) {
			my $cmd = $1;
			if($cmd eq 'tt') {
				$r .= '<code>';
				push @ends, '</code>';
			} elsif($cmd eq 'it') {
				$r .= '<i>';
				push @ends, '</i>';
			} elsif($cmd eq 'bf') {
				$r .= '<b>';
				push @ends, '</b>';
			} elsif($cmd eq 'sc') {
				$r .= '<span class="smallcaps">';
				push @ends, '</span>';
			} else {
				warn "Unrecognized {\\$cmd ...}";
			}
		} elsif(/\G\\l\{\}/gc) {
			$r .= 'ł';
		} elsif(/\G\\([`'^"H~ck=.druvts]|textsub\w+|textsuperimposetilde|textpolhook|[rvt=]\*)\s*\{([^{}\\]*)\}/gc) {
			my ($d, $t) = ($1, $2);
			if($DIACRITICS{$d.$t}) {
				$r .= $DIACRITICS{$d.$t};
			} elsif($DIACRITICS{$d}) {
				$t =~ tr
					{BFMVTDRJLgNGXKQPHSZC4IYE1098@35WU72OA6|":;'}
					{βɸɱʋθðɾʝʎɡŋɣχʁʕʔɦʃʒɕɥɪʏɛɨʉɘɵəɜɐɯʊɤʌɔɑɒǀˈːˑʼ} if $ipamode;
				my $td = $t . $DIACRITICS{$d};
				$td =~ s{(.)([͜͡])}{$2$1};
				$r .= $td;
			} else {
				warn "Unknown diacritic \\$d\{...}";
			}
		} elsif(/\G\\([`'^"H~ck=.druvts]|\|m|textsub\w+|textsuperimposetilde|textpolhook|[rvt=]\*)\s*\{/gc) {
			my ($d) = ($1);
			if($DIACRITICS{$d}) {
				push @ends, $DIACRITICS{$d};
			} else {
				warn "Unknown diacritic \\$d\{...";
			}
		} elsif(/\G\\([`'^"~=.]|\|[`'\[\]m]|[rv=~]\*)([^{}* \\]|\\[ij])/gc) {
			my ($d, $t) = ($1, $2);
			$t = 'i' if $t eq '\\i';
			$t = 'j' if $t eq '\\j';
			if($DIACRITICS{$d.$t}) {
				$r .= $DIACRITICS{$d.$t};
			} elsif($DIACRITICS{$d}) {
				$t =~ tr
					{BFMVTDRJLgNGXKQPHSZC4IYE1098@35WU72OA6|":;'}
					{βɸɱʋθðɾʝʎɡŋɣχʁʕʔɦʃʒɕɥɪʏɛɨʉɘɵəɜɐɯʊɤʌɔɑɒǀˈːˑʼ} if $ipamode;
				$r .= $t . $DIACRITICS{$d};
			} else {
				warn "Unknown diacritic \\$d";
			}
		} elsif(/\G\\([`'^"~=.]|[rvt=]\*?)\s+([^{}*\\])/gc) {
			my ($d, $t) = ($1, $2);
			if($DIACRITICS{$d.$t}) {
				$r .= $DIACRITICS{$d.$t};
			} elsif($DIACRITICS{$d}) {
				$t =~ tr
					{BFMVTDRJLgNGXKQPHSZC4IYE1098@35WU72OA6|":;'}
					{βɸɱʋθðɾʝʎɡŋɣχʁʕʔɦʃʒɕɥɪʏɛɨʉɘɵəɜɐɯʊɤʌɔɑɒǀˈːˑʼ} if $ipamode;
				$r .= $t . $DIACRITICS{$d};
			} else {
				warn "Unknown diacritic \\$d";
			}
		} elsif(/\G\\(begin|end)\s*\{(\w+)\}/gc) {
			my $sl = ($1 eq 'end'?'/':'');
			my $cmd = $2;
			if($cmd eq 'itemize') {
				$r .= "<${sl}ul>";
			} elsif($cmd eq 'enumerate') {
				$r .= "<${sl}ol>";
			} elsif($cmd eq 'document' || $cmd eq 'center') {
			} elsif($cmd eq 'tabular') {
				#print "# tabular $sl\n";
				if($sl ne '/') {
					/\G\{[^}]*\}/gc or warn "Missing tabular arg";
					$r .= "\x{e002}";
					$in_table = 1;
					$table = "<\nR ";
				} else {
					$in_table = 0;
					$table .= $r;
					#$table =~ s{\nR $}{}s;
					$table .= "\n>\n";
					$table =~ s{\x{e002}}{}g;
					$table =~ s{<p>}{}g;
					print $table;
					$r = '<p>';
				}
			} else {
				warn "Unrecognized \\$1\{$cmd}";
			}
		} elsif(/\G\\([A-Za-z]+)\s*/gc) {
			my $cmd = $1;
			if($cmd eq 'url') {
				/\G\{([^}]+)\}/gc or warn 'Missing URL';
				my $url = $1;
				$url =~ s/&/&amp;/gc;
				$url =~ s/</&lt;/gc;
				$url =~ s/"/&quot;/gc;
				$url =~ s/\\&/&/g;
				$url =~ s/\\backslash\s+/\\/g;
				$r .= qq{<a href="$url">$url</a>};
			} elsif($cmd eq 'hspace') {
				/\G\{([^}]*)\}/gc or warn "Missing hspace arg";
			} elsif($cmd eq 'raisebox') {
				/\G\{([^}]*)\}\s*\{/gc or warn "Missing raisebox arg";
				push @ends, '';
			} elsif($cmd =~ /^((?:sub)*)section$/) {
				# all sections are at the beginning of a line
				$r = "$1\x{e000}";
				/\G\{/gc or warn "Missing section arg";
				push @ends, "\x{e000}<p>";
			} elsif($cmd =~ /^((?:sub)*)paragraph$/) {
				# all sections are at the beginning of a line
				$r = "subsubsub$1\x{e000}";
				/\G\{/gc or warn "Missing paragraph arg";
				push @ends, "\x{e000}<p>";
			} elsif($cmd eq 'textbf') {
				/\G\{/gc or warn "Missing textbf arg";
				$r .= '<b>';
				push @ends, "</b>";
			} elsif($cmd eq 'textit') {
				/\G\{/gc or warn "Missing textit arg";
				$r .= '<i>';
				push @ends, "</i>";
			} elsif($cmd eq 'texttt') {
				/\G\{/gc or warn "Missing texttt arg";
				$r .= '<code>';
				push @ends, "</code>";
			} elsif($cmd eq 'ipa' || $cmd eq 'textipa') {
				if(/\G\{/gc) {
					$ipamode = 1;
					push @ends, "\x{e001}";
				} else {
					warn "Missing ipa arg";
				}
			} elsif($cmd eq 'super') {
				if(/\G\\textltailn/gc) {
					$r .= 'ᶮ';
					next;
				}
				/\G(.)/gc or warn "Missing super arg";
				my $ch = $1;
				warn "No super $ch" unless $SUPERS{$ch};
				$r .= $SUPERS{$ch};
			} elsif(defined($TEXT_CMDS{$cmd})) {
				$r .= $TEXT_CMDS{$cmd};
			} else {
				warn "Unrecognized \\$cmd";
			}
		} elsif(/\G\\!\{/gc) {
			# does nothing? no space?
			push @ends, '';
#		} elsif(/\G@\\textrhoticity/gc) {
#			$r .= 'ɚ';
		} elsif(/\G}/gc) {
			warn "Extra end ".pos if !@ends;
			if($ends[$#ends] eq "\x{e001}") {
				pop @ends;
				$ipamode = 0;
			} else {
				$r .= pop @ends;
				$r =~ s{(.)([͜͡])$}{$2$1};
			}
		} elsif(/\G\$(\^|_)([0-9nxus])\$/gc) {
			my $which = $1; my $ch = $2;
			if($which eq '^') {
				$r .= "<sup>$ch</sup>";
			} else {
				$r .= "<sub>$ch</sub>";
			}
		} elsif(/\G\$(\^|_)([0-9nxus])(\^|_)([0-9nxus])\$/gc) {
			my $which = $1.$3; my $ch1 = $2, $ch2 = $4;
			if($which eq '^_') {
				$r .= "<sub>$ch2</sub><sup>$ch1</sup>";
			} else {
				$r .= "<sub>$ch1</sub><sup>$ch2</sup>";
			}
		} elsif(/\G\\([ #\$%{}])/gc) {
			$r .= $1;
		} elsif(/\G\\&/gc) {
			$r .= '&amp;';
		} elsif(/\G\\\\/gc) {
			if($in_table) {
				$r .= "\nR ";
			} else {
				$r .= '<br>';
			}
		} elsif(/\G\&/gc && $in_table) {
			$r .= "\t";
		} elsif(/\G\\([!;*:].)/gc) {
			my $cmd = $1;
			if($TEXT_CMDS{$cmd}) {
				$r .= $TEXT_CMDS{$cmd};
			} else {
				warn "Unrecognized \\$cmd";
			}
		} elsif(/\G\$\\([A-Za-z]+)\$/gc) {
			my $cmd = $1;
			if($TEXT_CMDS{$cmd}) {
				$r .= $TEXT_CMDS{$cmd};
			} else {
				warn "Unrecognized \\$cmd";
			}
		} elsif(/\G([^\\{}\$&<>%]+)/gc) {
			my $text = $1;
			if($ipamode) {
				$text =~ s{""}{ˌ}g;
				$text =~ s{\|\|}{ǁ}g;
				$text =~ tr
					{BFMVTDRJLgNGXKQPHSZC4IYE1098@35WU72OA6|":;'}
					{βɸɱʋθðɾʝʎɡŋɣχʁʕʔɦʃʒɕɥɪʏɛɨʉɘɵəɜɐɯʊɤʌɔɑɒǀˈːˑʼ};
			}
			$text =~ s{``}{“}g;
			$text =~ s{''|"}{”}g;
			$text =~ s{`}{‘}g;
			$text =~ s{'}{’}g;
			$text =~ s{---}{—}g;
			$text =~ s{--}{–}g;
			$r .= $text;
		} elsif(/\G(?:\\-|\\,|\\$)/gc) {
			print STDERR ""; # ? - doesn't appear to do anything...
		} elsif(/\G\{/gc) {
			warn "Extra { at ",pos, "\n", hlchar($_, pos);
			push @ends, '';
		} elsif(/\G%/gc) {
			last;
		} else {
			warn "Unknown thing at ", pos, "\n", hlchar($_, pos);
			/\G./gc;
		}
		die "Infinite loop at ",pos if (pos) <= $prevpos;
		$prevpos = pos;
	}
	if($r =~ /\x{e002}/) {
		$table .= $'; $r = $`;
	}
	if(@ends) {
		warn "Missing ends @ends";
		#$r .= "\x{e003}";
	}
	if($r =~ /(<p>|<br>)$/) {
		$want_break = $&;
		$r =~ s///;
	}
	return $r;
}

my @section = 0;
while(<$in>) {
	#if(/^\\section\{([^}]+)\}/) {
	#	my $name = $1;
	#	$_ = $';
	#	print "S $name\n";
	#}
	chomp;
	#print STDERR $., "\n";
	my $line = $_;
	if(/^\s*$/) {
		$want_break = '<p>';
	}
	$_ = parseLaTeX($line);
	while(/\x{e003}/) {
		my $more = <$in>;
		die "Missing end" unless $more;
		$line .= " ".$more;
		chomp $line;
		$_ = parseLaTeX($line);
	}
	if(/^((?:sub)*)\x{e000}([^\x{e000}]*)\x{e000}/) {
		my $subs = $1; my $title = $2;
		$_ = $';
		$subs = (length($subs) / 3) + 1;
		#print "# $subs @section\n";
		pop @section while scalar(@section) > $subs;
		#print "# $subs @section\n";
		push @section, 0 while scalar(@section) < $subs;
		#print "# $subs @section\n";
		$section[$#section]++;
		$title =~ s{^<p>|^<br>}{};
		print "S ", join(".", @section), " $title\n";
	}
	next if /^\s*$/;
	if(/→/ && !/^<li/ && !/^(<p>|<br>)— At /) {
		s{<p>|<br>}{}g;
		$want_break = '<p>';
		print "C $_\n";
	} else {
		print "T $_\n";
	}
}
print STDERR "$warnings warnings\n";
