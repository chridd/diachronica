#! /usr/bin/perl -Ihtml/
use Diachronica;
use Encode;
binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

$Diachronica::DATA_FILE = 'html/diachronica-data';
$Diachronica::INDEX_FILE = 'html/diachronica-index';

my $query = parse_query(decode('utf-8', $ARGV[0]));

my $prev_main_section = -1;
my $prev_section = -1;

read_data(
	change => sub {
		my ($line, $id, %context) = @_;
		my %change = parse_sound_change($line);
		my $match = match_change($query, %change);
		if($match) {
			if($prev_main_section ne $context{main_num}) {
				print $context{main_name}, "\n";
				$prev_section = $prev_main_section = $context{main_num};
			}
			if($prev_section ne $context{num}) {
				print $context{name}, "\n";
				$prev_section = $context{num};
			}
			print $line, "\n";
		}
	},
	pchange => sub {
		my ($pline, %context) = @_;
		my %change = parse_sound_change_processed($pline);
		my ($line, $id) = @change{'html', 'id'};
		my $match = match_change($query, %change);
		if($match) {
			if($prev_main_section ne $context{main_num}) {
				print $context{main_name}, "\n";
				$prev_section = $prev_main_section = $context{main_num};
			}
			if($prev_section ne $context{num}) {
				print $context{name}, "\n";
				$prev_section = $context{num};
			}
			print $line, "\n";
		}
	},
	seeks => search_index($query)
);
