#! /usr/bin/perl -TI.
# ^ change the "." to wherever Diachronica.pm is located (e.g. -TI/whatever/)
use strict; use warnings; use utf8;
use Diachronica;
use CGI ('param');
use Encode;

# Change this number whenever the output changes to ensure the cache gets
# updated.  This can also be a string.  
my $VERSION = 10;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';
# change these to wherever the files are located on your server
$Diachronica::ALL = 'all.html'; # path as seen from the browser
$Diachronica::DATA_FILE = 'diachronica-data'; # path as seen from the server
$Diachronica::INDEX_FILE = 'diachronica-index'; # path as seen from the server
#$Diachronica::STYLESHEET = '<link rel="stylesheet" href="path/to/styles.css">';

# CGI scripts are supposed to check if the request method is correct
{
	my $method = $ENV{REQUEST_METHOD};
	if($method eq 'OPTIONS') {
		print qq{Status: 200 Options
Content-Length: 0
Allow: GET, HEAD

};
		exit 0;
	} elsif($method ne 'GET' && $method ne 'POST') {
		print qq{Status: 405 Method Not Allowed
Allow: GET, HEAD
Content-Type: text/html; charset=utf-8

<!doctype html>
$Diachronica::STYLESHEET<title>Error</title>
<h1>Error</h1>
<p>Your browser for some reason used the wrong request method.
};
		exit 0;
	}
}
# Don't repeat the search if the browser or proxy already has it cached
{
	my $etag = qq{"$VERSION"};
	if($etag !~ /^"[^"\n\r\0]*"$/) {
		die "Unsanitized etag";
	}
	print "ETag: $etag\n";
	if(index($ENV{HTTP_IF_NONE_MATCH}||'', $etag) >= 0) {
		print "Status: 304 Not modified\n\n";
		exit 0;
	}
}

my $query = param('q');
if(!defined($query)) {
	print qq{Status: 404 error
Content-Type: text/html; charset=utf-8

<!doctype html>
$Diachronica::STYLESHEET<title>Error</title>
<h1>Error</h1>
<p>No query has been entered.
};
	exit 0;
}

$query = parse_query(decode('utf8', $query));
print qq{Content-Type: text/html; charset=utf-8\n\n};

my $fquery = format_query('either', $query);
my $fquery_title = Diachronica::format_title($fquery);
# styles can go here
print qq{<!doctype html>
<meta charset="utf-8">
$Diachronica::STYLESHEET<title>Index Diachronica: $fquery_title</title>
<link rel="icon" href="icon.png">
<link rel="apple-touch-icon" href="icon.png">
<style>
td {vertical-align:top;}
#results a {text-decoration:none;} /* so diacritics and underscores are visible */
</style>

<h1>Index Diachronica: $fquery</h1>
<nav class=bc><p><a href="index.shtml">Searchable Index Diachronica</a> » <strong>Search</strong></p></nav>

<div id="results">
};
search_html($query);
print qq{</div>\n};
