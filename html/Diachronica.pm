package Diachronica;
use utf8;
use warnings;
use strict;
use Exporter 'import';
use Unicode::Normalize qw(NFC NFD checkNFC checkNFD);
use Data::Dumper;
our @EXPORT = qw(make_html make_toc make_html_of_sections find_sound_changes parse_sound_change parse_sound_change_processed open_data sounds parse_query format_query match_change read_data add_sounds_to_table make_sounds_table search_html normalize_sound search_index);

# C - sound change
# c id ^A text ^A from ^A to ^A context - preprocessed sound change
# 	changes separated by ^B
# 	alternatives separated by ^C
# S - section
# s id ^A text - preprocessed section
# T - text
# < R > - table
# ‹...› don't include in HTML output
# «...» only include in HTML output

# config
our $DATA_FILE = 'data.txt';
our $INDEX_FILE;
our $SEARCH_URL = 'search.cgi';
our $STYLESHEET = '';
#our $STYLESHEET = '<link rel="stylesheet" href="path/to/styles.css">';

my $INT_PATTERN = 'V';
my $INT_SIZE = 4;

my $REPORT_MISMATCHED = 0;

sub true {return 1;} sub false {return 0;}
our %IPA = ("a"=>{"sampa"=>"a","ipa"=>"a","wp"=>"Open_front_unrounded_vowel","desc"=>"open front unrounded vowel","type"=>"vowel","height"=>"open","backness"=>"front","rounding"=>"unrounded"},"b"=>{"sampa"=>"b","ipa"=>"b","wp"=>"Voiced_bilabial_plosive","desc"=>"voiced bilabial plosive","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"ɓ"=>{"sampa"=>"b_<","ipa"=>"ɓ","wp"=>"Voiced_bilabial_implosive","desc"=>"voiced bilabial implosive","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"c"=>{"sampa"=>"c","ipa"=>"c","wp"=>"Voiceless_palatal_plosive","desc"=>"voiceless palatal plosive","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"d"=>{"sampa"=>"d","ipa"=>"d","wp"=>"Voiced_alveolar_plosive","desc"=>"voiced alveolar plosive","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"ɖ"=>{"sampa"=>"d`","ipa"=>"ɖ","wp"=>"Voiced_retroflex_plosive","desc"=>"voiced retroflex plosive","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"ɗ"=>{"sampa"=>"d_<","ipa"=>"ɗ","wp"=>"Voiced_alveolar_implosive","desc"=>"voiced alveolar implosive","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"e"=>{"sampa"=>"e","ipa"=>"e","wp"=>"Close-mid_front_unrounded_vowel","desc"=>"close-mid front unrounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"front","rounding"=>"unrounded"},"f"=>{"sampa"=>"f","ipa"=>"f","wp"=>"Voiceless_labiodental_fricative","desc"=>"voiceless labiodental fricative","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ɡ"=>{"sampa"=>"g","ipa"=>"ɡ","wp"=>"Voiced_velar_plosive","desc"=>"voiced velar plosive","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"ɠ"=>{"sampa"=>"g_<","ipa"=>"ɠ","wp"=>"Voiced_velar_implosive","desc"=>"voiced velar implosive","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"h"=>{"sampa"=>"h","ipa"=>"h","wp"=>"Voiceless_glottal_fricative","desc"=>"voiceless glottal fricative","type"=>"consonant","poa"=>"glottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ɦ"=>{"sampa"=>"h\\","ipa"=>"ɦ","wp"=>"Voiced_glottal_fricative","desc"=>"voiced glottal fricative","type"=>"consonant","poa"=>"glottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"i"=>{"sampa"=>"i","ipa"=>"i","wp"=>"Close_front_unrounded_vowel","desc"=>"close front unrounded vowel","type"=>"vowel","height"=>"close","backness"=>"front","rounding"=>"unrounded"},"j"=>{"sampa"=>"j","ipa"=>"j","wp"=>"Palatal_approximant","desc"=>"palatal approximant","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ʝ"=>{"sampa"=>"j\\","ipa"=>"ʝ","wp"=>"Voiced_palatal_fricative","desc"=>"voiced palatal fricative","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"k"=>{"sampa"=>"k","ipa"=>"k","wp"=>"Voiceless_velar_plosive","desc"=>"voiceless velar plosive","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"l"=>{"sampa"=>"l","ipa"=>"l","wp"=>"Alveolar_lateral_approximant","desc"=>"alveolar lateral approximant","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ɭ"=>{"sampa"=>"l`","ipa"=>"ɭ","wp"=>"Retroflex_lateral_approximant","desc"=>"retroflex lateral approximant","type"=>"consonant","poa"=>"retroflex","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ɺ"=>{"sampa"=>"l\\","ipa"=>"ɺ","wp"=>"Alveolar_lateral_flap","desc"=>"alveolar lateral flap","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"flap","implosive"=>false,"voicing"=>"voiced"},"m"=>{"sampa"=>"m","ipa"=>"m","wp"=>"Bilabial_nasal","desc"=>"bilabial nasal","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"n"=>{"sampa"=>"n","ipa"=>"n","wp"=>"Alveolar_nasal","desc"=>"alveolar nasal","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"ɳ"=>{"sampa"=>"n`","ipa"=>"ɳ","wp"=>"Retroflex_nasal","desc"=>"retroflex nasal","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"o"=>{"sampa"=>"o","ipa"=>"o","wp"=>"Close-mid_back_rounded_vowel","desc"=>"close-mid back rounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"back","rounding"=>"rounded"},"p"=>{"sampa"=>"p","ipa"=>"p","wp"=>"Voiceless_bilabial_plosive","desc"=>"voiceless bilabial plosive","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"ɸ"=>{"sampa"=>"p\\","ipa"=>"ɸ","wp"=>"Voiceless_bilabial_fricative","desc"=>"voiceless bilabial fricative","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"q"=>{"sampa"=>"q","ipa"=>"q","wp"=>"Voiceless_uvular_plosive","desc"=>"voiceless uvular plosive","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"r"=>{"sampa"=>"r","ipa"=>"r","wp"=>"Alveolar_trill","desc"=>"alveolar trill","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"trill","implosive"=>false,"voicing"=>"voiced"},"ɽ"=>{"sampa"=>"r`","ipa"=>"ɽ","wp"=>"Retroflex_flap","desc"=>"retroflex flap","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"flap","implosive"=>false,"voicing"=>"voiced"},"ɹ"=>{"sampa"=>"r\\","ipa"=>"ɹ","wp"=>"Alveolar_approximant","desc"=>"alveolar approximant","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ɻ"=>{"sampa"=>"r\\`","ipa"=>"ɻ","wp"=>"Retroflex_approximant","desc"=>"retroflex approximant","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"s"=>{"sampa"=>"s","ipa"=>"s","wp"=>"Voiceless_alveolar_fricative","desc"=>"voiceless alveolar fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ʂ"=>{"sampa"=>"s`","ipa"=>"ʂ","wp"=>"Voiceless_retroflex_fricative","desc"=>"voiceless retroflex fricative","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ɕ"=>{"sampa"=>"s\\","ipa"=>"ɕ","wp"=>"Voiceless_alveolo-palatal_fricative","desc"=>"voiceless alveolo-palatal fricative","type"=>"consonant","poa"=>"alveolo-palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"t"=>{"sampa"=>"t","ipa"=>"t","wp"=>"Voiceless_alveolar_plosive","desc"=>"voiceless alveolar plosive","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"ʈ"=>{"sampa"=>"t`","ipa"=>"ʈ","wp"=>"Voiceless_retroflex_plosive","desc"=>"voiceless retroflex plosive","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"u"=>{"sampa"=>"u","ipa"=>"u","wp"=>"Close_back_rounded_vowel","desc"=>"close back rounded vowel","type"=>"vowel","height"=>"close","backness"=>"back","rounding"=>"rounded"},"v"=>{"sampa"=>"v","ipa"=>"v","wp"=>"Voiced_labiodental_fricative","desc"=>"voiced labiodental fricative","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʋ"=>{"sampa"=>"P","ipa"=>"ʋ","wp"=>"Labiodental_approximant","desc"=>"labiodental approximant","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"w"=>{"sampa"=>"w","ipa"=>"w","wp"=>"Labial-velar_approximant","desc"=>"labial-velar approximant","type"=>"consonant","rounding"=>"rounded","poa"=>"velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"x"=>{"sampa"=>"x","ipa"=>"x","wp"=>"Voiceless_velar_fricative","desc"=>"voiceless velar fricative","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ɧ"=>{"sampa"=>"x\\","ipa"=>"ɧ","wp"=>"Voiceless_palatal-velar_fricative","desc"=>"voiceless palatal-velar fricative","type"=>"consonant","poa"=>"palatal-velar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"y"=>{"sampa"=>"y","ipa"=>"y","wp"=>"Close_front_rounded_vowel","desc"=>"close front rounded vowel","type"=>"vowel","height"=>"close","backness"=>"front","rounding"=>"rounded"},"z"=>{"sampa"=>"z","ipa"=>"z","wp"=>"Voiced_alveolar_fricative","desc"=>"voiced alveolar fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʐ"=>{"sampa"=>"z`","ipa"=>"ʐ","wp"=>"Voiced_retroflex_fricative","desc"=>"voiced retroflex fricative","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʑ"=>{"sampa"=>"z\\","ipa"=>"ʑ","wp"=>"Voiced_alveolo-palatal_fricative","desc"=>"voiced alveolo-palatal fricative","type"=>"consonant","poa"=>"alveolo-palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ɑ"=>{"sampa"=>"A","ipa"=>"ɑ","wp"=>"Open_back_unrounded_vowel","desc"=>"open back unrounded vowel","type"=>"vowel","height"=>"open","backness"=>"back","rounding"=>"unrounded"},"β"=>{"sampa"=>"B","ipa"=>"β","wp"=>"Voiced_bilabial_fricative","desc"=>"voiced bilabial fricative","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʙ"=>{"sampa"=>"B\\","ipa"=>"ʙ","wp"=>"Bilabial_trill","desc"=>"bilabial trill","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"trill","implosive"=>false,"voicing"=>"voiced"},"ç"=>{"sampa"=>"C","ipa"=>"ç","wp"=>"Voiceless_palatal_fricative","desc"=>"voiceless palatal fricative","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ð"=>{"sampa"=>"D","ipa"=>"ð","wp"=>"Voiced_dental_fricative","desc"=>"voiced dental fricative","type"=>"consonant","poa"=>"dental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ɛ"=>{"sampa"=>"E","ipa"=>"ɛ","wp"=>"Open-mid_front_unrounded_vowel","desc"=>"open-mid front unrounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"front","rounding"=>"unrounded"},"ɱ"=>{"sampa"=>"F","ipa"=>"ɱ","wp"=>"Labiodental_nasal","desc"=>"labiodental nasal","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"ɣ"=>{"sampa"=>"G","ipa"=>"ɣ","wp"=>"Voiced_velar_fricative","desc"=>"voiced velar fricative","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ɢ"=>{"sampa"=>"G\\","ipa"=>"ɢ","wp"=>"Voiced_uvular_plosive","desc"=>"voiced uvular plosive","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"ʛ"=>{"sampa"=>"G\\_<","ipa"=>"ʛ","wp"=>"Voiced_uvular_implosive","desc"=>"voiced uvular implosive","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"ɥ"=>{"sampa"=>"H","ipa"=>"ɥ","wp"=>"Labial-palatal_approximant","desc"=>"labial-palatal approximant","type"=>"consonant","rounding"=>"rounded","poa"=>"palatal","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ʜ"=>{"sampa"=>"H\\","ipa"=>"ʜ","wp"=>"Voiceless_epiglottal_fricative","desc"=>"voiceless epiglottal fricative","type"=>"consonant","poa"=>"epiglottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ɪ"=>{"sampa"=>"I","ipa"=>"ɪ","wp"=>"Near-close_near-front_unrounded_vowel","desc"=>"near-close near-front unrounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"near-front","rounding"=>"unrounded"},"ᵻ"=>{"sampa"=>"I\\","ipa"=>"ᵻ","wp"=>"Near-close_central_unrounded_vowel","desc"=>"near-close central unrounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"central","rounding"=>"unrounded"},"ɲ"=>{"sampa"=>"J","ipa"=>"ɲ","wp"=>"Palatal_nasal","desc"=>"palatal nasal","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"ɟ"=>{"sampa"=>"J\\","ipa"=>"ɟ","wp"=>"Voiced_palatal_plosive","desc"=>"voiced palatal plosive","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"ʄ"=>{"sampa"=>"J\\_<","ipa"=>"ʄ","wp"=>"Voiced_palatal_implosive","desc"=>"voiced palatal implosive","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"ɬ"=>{"sampa"=>"K","ipa"=>"ɬ","wp"=>"Voiceless_alveolar_lateral_fricative","desc"=>"voiceless alveolar lateral fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ɮ"=>{"sampa"=>"K\\","ipa"=>"ɮ","wp"=>"Voiced_alveolar_lateral_fricative","desc"=>"voiced alveolar lateral fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʎ"=>{"sampa"=>"L","ipa"=>"ʎ","wp"=>"Palatal_lateral_approximant","desc"=>"palatal lateral approximant","type"=>"consonant","poa"=>"palatal","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ʟ"=>{"sampa"=>"L\\","ipa"=>"ʟ","wp"=>"Velar_lateral_approximant","desc"=>"velar lateral approximant","type"=>"consonant","poa"=>"velar","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ɯ"=>{"sampa"=>"M","ipa"=>"ɯ","wp"=>"Close_back_unrounded_vowel","desc"=>"close back unrounded vowel","type"=>"vowel","height"=>"close","backness"=>"back","rounding"=>"unrounded"},"ɰ"=>{"sampa"=>"M\\","ipa"=>"ɰ","wp"=>"Velar_approximant","desc"=>"velar approximant","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"ŋ"=>{"sampa"=>"N","ipa"=>"ŋ","wp"=>"Velar_nasal","desc"=>"velar nasal","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"ɴ"=>{"sampa"=>"N\\","ipa"=>"ɴ","wp"=>"Uvular_nasal","desc"=>"uvular nasal","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"ɔ"=>{"sampa"=>"O","ipa"=>"ɔ","wp"=>"Open-mid_back_rounded_vowel","desc"=>"open-mid back rounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"back","rounding"=>"rounded"},"ʘ"=>{"sampa"=>"O\\","ipa"=>"ʘ","wp"=>"Bilabial_click","desc"=>"bilabial click","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"ɒ"=>{"sampa"=>"Q","ipa"=>"ɒ","wp"=>"Open_back_rounded_vowel","desc"=>"open back rounded vowel","type"=>"vowel","height"=>"open","backness"=>"back","rounding"=>"rounded"},"ʁ"=>{"sampa"=>"R","ipa"=>"ʁ","wp"=>"Voiced_uvular_fricative","desc"=>"voiced uvular fricative","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʀ"=>{"sampa"=>"R\\","ipa"=>"ʀ","wp"=>"Uvular_trill","desc"=>"uvular trill","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"trill","implosive"=>false,"voicing"=>"voiced"},"ʃ"=>{"sampa"=>"S","ipa"=>"ʃ","wp"=>"Voiceless_postalveolar_fricative","desc"=>"voiceless postalveolar fricative","type"=>"consonant","poa"=>"postalveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"θ"=>{"sampa"=>"T","ipa"=>"θ","wp"=>"Voiceless_dental_fricative","desc"=>"voiceless dental fricative","type"=>"consonant","poa"=>"dental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ʊ"=>{"sampa"=>"U","ipa"=>"ʊ","wp"=>"Near-close_near-back_rounded_vowel","desc"=>"near-close near-back rounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"near-back","rounding"=>"rounded"},"ᵿ"=>{"sampa"=>"U\\","ipa"=>"ᵿ","wp"=>"Near-close_central_rounded_vowel","desc"=>"near-close central rounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"central","rounding"=>"rounded"},"ʌ"=>{"sampa"=>"V","ipa"=>"ʌ","wp"=>"Open-mid_back_unrounded_vowel","desc"=>"open-mid back unrounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"back","rounding"=>"unrounded"},"ʍ"=>{"sampa"=>"W","ipa"=>"ʍ","wp"=>"Voiceless_labial-velar_fricative","desc"=>"voiceless labial-velar fricative","type"=>"consonant","rounding"=>"rounded","poa"=>"velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiceless"},"χ"=>{"sampa"=>"X","ipa"=>"χ","wp"=>"Voiceless_uvular_fricative","desc"=>"voiceless uvular fricative","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ħ"=>{"sampa"=>"X\\","ipa"=>"ħ","wp"=>"Voiceless_pharyngeal_fricative","desc"=>"voiceless pharyngeal fricative","type"=>"consonant","poa"=>"pharyngeal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"ʏ"=>{"sampa"=>"Y","ipa"=>"ʏ","wp"=>"Near-close_near-front_rounded_vowel","desc"=>"near-close near-front rounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"near-front","rounding"=>"rounded"},"ʒ"=>{"sampa"=>"Z","ipa"=>"ʒ","wp"=>"Voiced_postalveolar_fricative","desc"=>"voiced postalveolar fricative","type"=>"consonant","poa"=>"postalveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"."=>{"sampa"=>".","ipa"=>".","desc"=>"syllable break","type"=>"nonsound","nonsound"=>"syllable"},"ˈ"=>{"sampa"=>"\"","ipa"=>"ˈ","desc"=>"primary stress","wp"=>"Lexical_stress","type"=>"nonsound","nonsound"=>"primary"},"ˌ"=>{"sampa"=>"%","ipa"=>"ˌ","wp"=>"Secondary_stress","desc"=>"secondary stress","type"=>"nonsound","nonsound"=>"secondary"},"ʲ"=>{"sampa"=>"_j","ipa"=>"ʲ","wp"=>"Palatalization","desc"=>"palatalized","type"=>"mod","menu"=>"back-coarticulation","value"=>"palatalized"},"ː"=>{"sampa"=>"=>","ipa"=>"ː","desc"=>"long","type"=>"mod","menu"=>"length","value"=>"long"},"ˑ"=>{"sampa"=>"=>\\","ipa"=>"ˑ","desc"=>"half long","type"=>"mod","menu"=>"length","value"=>"half"},"ə"=>{"sampa"=>"@","ipa"=>"ə","wp"=>"Schwa","type"=>"vowel","rounding"=>"unrounded","height"=>"mid","backness"=>"central","desc"=>"schwa"},"ɘ"=>{"sampa"=>"@\\","ipa"=>"ɘ","wp"=>"Close-mid_central_unrounded_vowel","desc"=>"close-mid central unrounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"central","rounding"=>"unrounded"},"æ"=>{"sampa"=>"{","ipa"=>"æ","wp"=>"Near-open_front_unrounded_vowel","desc"=>"near-open front unrounded vowel","type"=>"vowel","height"=>"near-open","backness"=>"front","rounding"=>"unrounded"},"ʉ"=>{"sampa"=>"}","ipa"=>"ʉ","wp"=>"Close_central_rounded_vowel","desc"=>"close central rounded vowel","type"=>"vowel","height"=>"close","backness"=>"central","rounding"=>"rounded"},"ɨ"=>{"sampa"=>"1","ipa"=>"ɨ","wp"=>"Close_central_unrounded_vowel","desc"=>"close central unrounded vowel","type"=>"vowel","height"=>"close","backness"=>"central","rounding"=>"unrounded"},"ø"=>{"sampa"=>"2","ipa"=>"ø","wp"=>"Close-mid_front_rounded_vowel","desc"=>"close-mid front rounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"front","rounding"=>"rounded"},"ɜ"=>{"sampa"=>"3","ipa"=>"ɜ","wp"=>"Open-mid_central_unrounded_vowel","desc"=>"open-mid central unrounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"central","rounding"=>"unrounded"},"ɞ"=>{"sampa"=>"3\\","ipa"=>"ɞ","wp"=>"Open-mid_central_rounded_vowel","desc"=>"open-mid central rounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"central","rounding"=>"rounded"},"ɾ"=>{"sampa"=>"4","ipa"=>"ɾ","wp"=>"Alveolar_flap","desc"=>"alveolar flap","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"flap","implosive"=>false,"voicing"=>"voiced"},"ɫ"=>{"sampa"=>"5","ipa"=>"ɫ","wp"=>"Velarized_alveolar_lateral_approximant","type"=>"consonant","poa"=>"alveolar","moa"=>"approximant","lateral"=>true,"voicing"=>"voiced","coart"=>"velarized","desc"=>"velarized alveolar lateral approximant"},"ɐ"=>{"sampa"=>"6","ipa"=>"ɐ","wp"=>"Near-open_central_vowel","type"=>"vowel","rounding"=>"unrounded","height"=>"near-open","backness"=>"central","desc"=>"near-open central vowel"},"ɤ"=>{"sampa"=>"7","ipa"=>"ɤ","wp"=>"Close-mid_back_unrounded_vowel","desc"=>"close-mid back unrounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"back","rounding"=>"unrounded"},"ɵ"=>{"sampa"=>"8","ipa"=>"ɵ","wp"=>"Close-mid_central_rounded_vowel","desc"=>"close-mid central rounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"central","rounding"=>"rounded"},"œ"=>{"sampa"=>"9","ipa"=>"œ","wp"=>"Open-mid_front_rounded_vowel","desc"=>"open-mid front rounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"front","rounding"=>"rounded"},"ɶ"=>{"sampa"=>"&","ipa"=>"ɶ","wp"=>"Open_front_rounded_vowel","desc"=>"open front rounded vowel","type"=>"vowel","height"=>"open","backness"=>"front","rounding"=>"rounded"},"ʔ"=>{"sampa"=>"?","ipa"=>"ʔ","wp"=>"Glottal_stop","type"=>"consonant","poa"=>"glottal","moa"=>"plosive","voicing"=>"voiceless","implosive"=>false,"desc"=>"glottal stop"},"ʕ"=>{"sampa"=>"?\\","ipa"=>"ʕ","wp"=>"Voiced_pharyngeal_fricative","desc"=>"voiced pharyngeal fricative","type"=>"consonant","poa"=>"pharyngeal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʢ"=>{"sampa"=>"<\\","ipa"=>"ʢ","wp"=>"Voiced_epiglottal_fricative","desc"=>"voiced epiglottal fricative","type"=>"consonant","poa"=>"epiglottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"ʡ"=>{"sampa"=>">\\","ipa"=>"ʡ","wp"=>"Epiglottal_plosive","desc"=>"epiglottal plosive","type"=>"consonant","poa"=>"epiglottal","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"ꜛ"=>{"sampa"=>"^","ipa"=>"ꜛ","wp"=>"Upstep_(phonetics)","desc"=>"upstep","type"=>"nonsound","nonsound"=>"upstep"},"ꜜ"=>{"sampa"=>"!","ipa"=>"ꜜ","wp"=>"Downstep_(phonetics)","desc"=>"downstep","type"=>"nonsound","nonsound"=>"downstep"},"ǃ"=>{"sampa"=>"!\\","ipa"=>"ǃ","wp"=>"Postalveolar_click","desc"=>"postalveolar click","type"=>"consonant","poa"=>"postalveolar","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"|"=>{"sampa"=>"|","ipa"=>"|","desc"=>"minor (foot) group","type"=>"nonsound","nonsound"=>"minor"},"ǀ"=>{"sampa"=>"|\\","ipa"=>"ǀ","wp"=>"Dental_click","desc"=>"dental click","type"=>"consonant","poa"=>"dental","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"‖"=>{"sampa"=>"||","ipa"=>"‖","desc"=>"major (intonation) group","type"=>"nonsound","nonsound"=>"major"},"ǁ"=>{"sampa"=>"|\\|\\","ipa"=>"ǁ","wp"=>"Alveolar_lateral_click","desc"=>"alveolar lateral click","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"ǂ"=>{"sampa"=>"=\\","ipa"=>"ǂ","wp"=>"Palatal_click","desc"=>"palatal click","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"‿"=>{"sampa"=>"-\\","ipa"=>"‿","wp"=>"Liaison_(linguistics)","desc"=>"linking mark","type"=>"nonsound","nonsound"=>"linking"}," ̈"=>{"sampa"=>"_\"","ipa"=>" ̈","wp"=>"Centralization_(phonetics)","desc"=>"centralized","type"=>"mod","menu"=>"centralization","value"=>"centralized"},"̈"=>{"sampa"=>"_\"","ipa"=>" ̈","wp"=>"Centralization_(phonetics)","desc"=>"centralized","type"=>"mod","menu"=>"centralization","value"=>"centralized"}," ̟"=>{"sampa"=>"_+","ipa"=>" ̟","wp"=>"Advanced_(phonetics)","desc"=>"advanced","type"=>"mod","menu"=>"advancedness","value"=>"advanced"},"̟"=>{"sampa"=>"_+","ipa"=>" ̟","wp"=>"Advanced_(phonetics)","desc"=>"advanced","type"=>"mod","menu"=>"advancedness","value"=>"advanced"}," ̠"=>{"sampa"=>"_-","ipa"=>" ̠","wp"=>"Retracted_(phonetics)","desc"=>"retracted","type"=>"mod","menu"=>"advancedness","value"=>"retracted"},"̠"=>{"sampa"=>"_-","ipa"=>" ̠","wp"=>"Retracted_(phonetics)","desc"=>"retracted","type"=>"mod","menu"=>"advancedness","value"=>"retracted"}," ̌"=>{"sampa"=>"_R","ipa"=>" ̌","desc"=>"rising tone","type"=>"mod","menu"=>"tone","value"=>"rising"},"̌"=>{"sampa"=>"_R","ipa"=>" ̌","desc"=>"rising tone","type"=>"mod","menu"=>"tone","value"=>"rising"}," ̥"=>{"sampa"=>"_0","ipa"=>" ̥","wp"=>"Voiceless_consonant","desc"=>"voiceless","type"=>"mod","menu"=>"voicing","value"=>"voiceless"},"̥"=>{"sampa"=>"_0","ipa"=>" ̥","wp"=>"Voiceless_consonant","desc"=>"voiceless","type"=>"mod","menu"=>"voicing","value"=>"voiceless"},"̊"=>{"sampa"=>"_0","ipa"=>" ̥","wp"=>"Voiceless_consonant","desc"=>"voiceless","type"=>"mod","menu"=>"voicing","value"=>"voiceless"},"(implosive)"=>{"sampa"=>"_<","ipa"=>"(implosive)","wp"=>"Implosive_consonant","desc"=>"implosive","type"=>"mod","menu"=>"ejective","value"=>"implosive"}," ̩"=>{"sampa"=>"=","ipa"=>" ̩","desc"=>"syllabic","type"=>"mod","checkbox"=>"syllabic","value"=>true},"̩"=>{"sampa"=>"=","ipa"=>" ̩","desc"=>"syllabic","type"=>"mod","checkbox"=>"syllabic","value"=>true},"ʼ"=>{"sampa"=>"_>","ipa"=>"ʼ","wp"=>"Ejective_consonant","desc"=>"ejective","type"=>"mod","menu"=>"ejective","value"=>"ejective"},"ˤ"=>{"sampa"=>"_?\\","ipa"=>"ˤ","wp"=>"Pharyngealisation","desc"=>"pharyngealized","type"=>"mod","menu"=>"back-coarticulation","value"=>"pharyngealized"}," ̂"=>{"sampa"=>"_F","ipa"=>" ̂","desc"=>"falling tone","type"=>"mod","menu"=>"tone","value"=>"falling"},"̂"=>{"sampa"=>"_F","ipa"=>" ̂","desc"=>"falling tone","type"=>"mod","menu"=>"tone","value"=>"falling"}," ̯"=>{"sampa"=>"_^","ipa"=>" ̯","desc"=>"non-syllabic","type"=>"mod","checkbox"=>"syllabic","value"=>false},"̯"=>{"sampa"=>"_^","ipa"=>" ̯","desc"=>"non-syllabic","type"=>"mod","checkbox"=>"syllabic","value"=>false}," ̚"=>{"sampa"=>"_}","ipa"=>" ̚","wp"=>"No_audible_release","type"=>"mod","menu"=>"unreleased","value"=>"unreleased","desc"=>"no audible release"},"̚"=>{"sampa"=>"_}","ipa"=>" ̚","wp"=>"No_audible_release","type"=>"mod","menu"=>"unreleased","value"=>"unreleased","desc"=>"no audible release"},"˞"=>{"sampa"=>"`","ipa"=>"˞","wp"=>"R-colored_vowel","desc"=>"rhotacization","type"=>"mod","checkbox"=>"rhotacization","value"=>true}," ̃"=>{"sampa"=>"~","ipa"=>" ̃","wp"=>"Nasalization","desc"=>"nasalization","type"=>"mod","checkbox"=>"nasalization","value"=>true},"̃"=>{"sampa"=>"~","ipa"=>" ̃","wp"=>"Nasalization","desc"=>"nasalization","type"=>"mod","checkbox"=>"nasalization","value"=>true}," ̘"=>{"sampa"=>"_A","ipa"=>" ̘","wp"=>"Advanced_tongue_root","type"=>"mod","menu"=>"atr","value"=>"advanced","desc"=>"advanced tongue root"},"̘"=>{"sampa"=>"_A","ipa"=>" ̘","wp"=>"Advanced_tongue_root","type"=>"mod","menu"=>"atr","value"=>"advanced","desc"=>"advanced tongue root"}," ̺"=>{"sampa"=>"_a","ipa"=>" ̺","wp"=>"Apical_consonant","desc"=>"apical","type"=>"mod","menu"=>"apical","value"=>"apical"},"̺"=>{"sampa"=>"_a","ipa"=>" ̺","wp"=>"Apical_consonant","desc"=>"apical","type"=>"mod","menu"=>"apical","value"=>"apical"}," ̏"=>{"sampa"=>"_B","ipa"=>" ̏","desc"=>"extra low tone","type"=>"mod","menu"=>"tone","value"=>"extra-low"},"̏"=>{"sampa"=>"_B","ipa"=>" ̏","desc"=>"extra low tone","type"=>"mod","menu"=>"tone","value"=>"extra-low"}," ᷅"=>{"sampa"=>"_B_L","ipa"=>" ᷅","desc"=>"low rising tone","type"=>"mod","menu"=>"tone","value"=>"low-rising"},"᷅"=>{"sampa"=>"_B_L","ipa"=>" ᷅","desc"=>"low rising tone","type"=>"mod","menu"=>"tone","value"=>"low-rising"}," ̜"=>{"sampa"=>"_c","ipa"=>" ̜","desc"=>"less rounded","type"=>"mod","menu"=>"rounding","value"=>"less"},"̜"=>{"sampa"=>"_c","ipa"=>" ̜","desc"=>"less rounded","type"=>"mod","menu"=>"rounding","value"=>"less"}," ̪"=>{"sampa"=>"_d","ipa"=>" ̪","wp"=>"Dental_consonant","desc"=>"dental","type"=>"mod","menu"=>"poa","value"=>"dental"},"̪"=>{"sampa"=>"_d","ipa"=>" ̪","wp"=>"Dental_consonant","desc"=>"dental","type"=>"mod","menu"=>"poa","value"=>"dental"}," ̴"=>{"sampa"=>"_e","ipa"=>" ̴","desc"=>"velarized or pharyngealized","type"=>"mod","menu"=>"back-coarticulation","value"=>"velarized"},"̴"=>{"sampa"=>"_e","ipa"=>" ̴","desc"=>"velarized or pharyngealized","type"=>"mod","menu"=>"back-coarticulation","value"=>"velarized"},"↘"=>{"sampa"=>"<F>","ipa"=>"↘","wp"=>"Intonation_(linguistics)","desc"=>"global fall","type"=>"nonsound","nonsound"=>"fall"},"ˠ"=>{"sampa"=>"_G","ipa"=>"ˠ","wp"=>"Velarization","desc"=>"velarized","type"=>"mod","menu"=>"back-coarticulation","value"=>"velarized"}," ́"=>{"sampa"=>"_H","ipa"=>" ́","desc"=>"high tone","type"=>"mod","menu"=>"tone","value"=>"high"},"́"=>{"sampa"=>"_H","ipa"=>" ́","desc"=>"high tone","type"=>"mod","menu"=>"tone","value"=>"high"}," ᷄"=>{"sampa"=>"_H_T","ipa"=>" ᷄","desc"=>"high rising tone","type"=>"mod","menu"=>"tone","value"=>"high-rising"},"᷄"=>{"sampa"=>"_H_T","ipa"=>" ᷄","desc"=>"high rising tone","type"=>"mod","menu"=>"tone","value"=>"high-rising"},"ʰ"=>{"sampa"=>"_h","ipa"=>"ʰ","wp"=>"Aspiration_(phonetics)","desc"=>"aspirated","type"=>"mod","checkbox"=>"aspirated","value"=>true}," ̰"=>{"sampa"=>"_k","ipa"=>" ̰","wp"=>"Creaky_voice","desc"=>"creaky voice","type"=>"mod","menu"=>"voicing","value"=>"creaky"},"̰"=>{"sampa"=>"_k","ipa"=>" ̰","wp"=>"Creaky_voice","desc"=>"creaky voice","type"=>"mod","menu"=>"voicing","value"=>"creaky"}," ̀"=>{"sampa"=>"_L","ipa"=>" ̀","desc"=>"low tone","type"=>"mod","menu"=>"tone","value"=>"low"},"̀"=>{"sampa"=>"_L","ipa"=>" ̀","desc"=>"low tone","type"=>"mod","menu"=>"tone","value"=>"low"},"ˡ"=>{"sampa"=>"_l","ipa"=>"ˡ","wp"=>"Lateral_release_(phonetics)","desc"=>"lateral release","type"=>"mod","checkbox"=>"lateral","value"=>true}," ̄"=>{"sampa"=>"_M","ipa"=>" ̄","desc"=>"mid tone","type"=>"mod","menu"=>"tone","value"=>"mid"},"̄"=>{"sampa"=>"_M","ipa"=>" ̄","desc"=>"mid tone","type"=>"mod","menu"=>"tone","value"=>"mid"}," ̻"=>{"sampa"=>"_m","ipa"=>" ̻","wp"=>"Laminal_consonant","desc"=>"laminal","type"=>"mod","menu"=>"apical","value"=>"laminal"},"̻"=>{"sampa"=>"_m","ipa"=>" ̻","wp"=>"Laminal_consonant","desc"=>"laminal","type"=>"mod","menu"=>"apical","value"=>"laminal"}," ̼"=>{"sampa"=>"_N","ipa"=>" ̼","wp"=>"Linguolabial_consonant","desc"=>"linguolabial","type"=>"mod","menu"=>"poa","value"=>"linguolabial"},"̼"=>{"sampa"=>"_N","ipa"=>" ̼","wp"=>"Linguolabial_consonant","desc"=>"linguolabial","type"=>"mod","menu"=>"poa","value"=>"linguolabial"},"ⁿ"=>{"sampa"=>"_n","ipa"=>"ⁿ","wp"=>"Nasal_release","type"=>"mod","menu"=>"moa","value"=>"prestopped","desc"=>"nasal release"}," ̹"=>{"sampa"=>"_O","ipa"=>" ̹","wp"=>"Roundedness","desc"=>"more rounded","type"=>"mod","menu"=>"rounding","value"=>"more"},"̹"=>{"sampa"=>"_O","ipa"=>" ̹","wp"=>"Roundedness","desc"=>"more rounded","type"=>"mod","menu"=>"rounding","value"=>"more"}," ̞"=>{"sampa"=>"_o","ipa"=>" ̞","wp"=>"Relative_articulation#Raised_and_lowered","desc"=>"lowered","type"=>"mod","menu"=>"raisedness","value"=>"lowered"},"̞"=>{"sampa"=>"_o","ipa"=>" ̞","wp"=>"Relative_articulation#Raised_and_lowered","desc"=>"lowered","type"=>"mod","menu"=>"raisedness","value"=>"lowered"}," ̙"=>{"sampa"=>"_q","ipa"=>" ̙","wp"=>"Retracted_tongue_root","type"=>"mod","menu"=>"atr","value"=>"retracted","desc"=>"retracted tongue root"},"̙"=>{"sampa"=>"_q","ipa"=>" ̙","wp"=>"Retracted_tongue_root","type"=>"mod","menu"=>"atr","value"=>"retracted","desc"=>"retracted tongue root"},"↗"=>{"sampa"=>"<R>","ipa"=>"↗","wp"=>"Intonation_(linguistics)","desc"=>"global rise","type"=>"nonsound","nonsound"=>"rise"}," ᷈"=>{"sampa"=>"_R_F","ipa"=>" ᷈","desc"=>"rising falling tone","type"=>"mod","menu"=>"tone","value"=>"rising-falling"},"᷈"=>{"sampa"=>"_R_F","ipa"=>" ᷈","desc"=>"rising falling tone","type"=>"mod","menu"=>"tone","value"=>"rising-falling"}," ̝"=>{"sampa"=>"_r","ipa"=>" ̝","wp"=>"Relative_articulation#Raised_and_lowered","desc"=>"raised","type"=>"mod","menu"=>"raisedness","value"=>"raised"},"̝"=>{"sampa"=>"_r","ipa"=>" ̝","wp"=>"Relative_articulation#Raised_and_lowered","desc"=>"raised","type"=>"mod","menu"=>"raisedness","value"=>"raised"}," ̋"=>{"sampa"=>"_T","ipa"=>" ̋","desc"=>"extra high tone","type"=>"mod","menu"=>"tone","value"=>"extra-high"},"̋"=>{"sampa"=>"_T","ipa"=>" ̋","desc"=>"extra high tone","type"=>"mod","menu"=>"tone","value"=>"extra-high"}," ̤"=>{"sampa"=>"_t","ipa"=>" ̤","wp"=>"Breathy_voice","desc"=>"breathy voice","type"=>"mod","menu"=>"voicing","value"=>"breathy"},"̤"=>{"sampa"=>"_t","ipa"=>" ̤","wp"=>"Breathy_voice","desc"=>"breathy voice","type"=>"mod","menu"=>"voicing","value"=>"breathy"}," ̬"=>{"sampa"=>"_v","ipa"=>" ̬","wp"=>"Voiced_consonant","desc"=>"voiced","type"=>"mod","menu"=>"voicing","value"=>"voiced"},"̬"=>{"sampa"=>"_v","ipa"=>" ̬","wp"=>"Voiced_consonant","desc"=>"voiced","type"=>"mod","menu"=>"voicing","value"=>"voiced"},"ʷ"=>{"sampa"=>"_w","ipa"=>"ʷ","wp"=>"Labialisation","desc"=>"labialized","type"=>"mod","menu"=>"rounding","value"=>"rounded"}," ̆"=>{"sampa"=>"_X","ipa"=>" ̆","desc"=>"extra-short","type"=>"mod","menu"=>"length","value"=>"extra-short"},"̆"=>{"sampa"=>"_X","ipa"=>" ̆","desc"=>"extra-short","type"=>"mod","menu"=>"length","value"=>"extra-short"}," ̽"=>{"sampa"=>"_x","ipa"=>" ̽","desc"=>"mid-centralized","type"=>"mod","menu"=>"centralization","value"=>"mid-centralized"},"̽"=>{"sampa"=>"_x","ipa"=>" ̽","desc"=>"mid-centralized","type"=>"mod","menu"=>"centralization","value"=>"mid-centralized"},"g"=>{"sampa"=>"g","ipa"=>"ɡ","wp"=>"Voiced_velar_plosive","desc"=>"voiced velar plosive","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"});
our %SAMPA = ("1"=>{"sampa"=>"1","ipa"=>"ɨ","wp"=>"Close_central_unrounded_vowel","desc"=>"close central unrounded vowel","type"=>"vowel","height"=>"close","backness"=>"central","rounding"=>"unrounded"},"2"=>{"sampa"=>"2","ipa"=>"ø","wp"=>"Close-mid_front_rounded_vowel","desc"=>"close-mid front rounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"front","rounding"=>"rounded"},"3"=>{"sampa"=>"3","ipa"=>"ɜ","wp"=>"Open-mid_central_unrounded_vowel","desc"=>"open-mid central unrounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"central","rounding"=>"unrounded"},"4"=>{"sampa"=>"4","ipa"=>"ɾ","wp"=>"Alveolar_flap","desc"=>"alveolar flap","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"flap","implosive"=>false,"voicing"=>"voiced"},"5"=>{"sampa"=>"5","ipa"=>"ɫ","wp"=>"Velarized_alveolar_lateral_approximant","type"=>"consonant","poa"=>"alveolar","moa"=>"approximant","lateral"=>true,"voicing"=>"voiced","coart"=>"velarized","desc"=>"velarized alveolar lateral approximant"},"6"=>{"sampa"=>"6","ipa"=>"ɐ","wp"=>"Near-open_central_vowel","type"=>"vowel","rounding"=>"unrounded","height"=>"near-open","backness"=>"central","desc"=>"near-open central vowel"},"7"=>{"sampa"=>"7","ipa"=>"ɤ","wp"=>"Close-mid_back_unrounded_vowel","desc"=>"close-mid back unrounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"back","rounding"=>"unrounded"},"8"=>{"sampa"=>"8","ipa"=>"ɵ","wp"=>"Close-mid_central_rounded_vowel","desc"=>"close-mid central rounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"central","rounding"=>"rounded"},"9"=>{"sampa"=>"9","ipa"=>"œ","wp"=>"Open-mid_front_rounded_vowel","desc"=>"open-mid front rounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"front","rounding"=>"rounded"},"a"=>{"sampa"=>"a","ipa"=>"a","wp"=>"Open_front_unrounded_vowel","desc"=>"open front unrounded vowel","type"=>"vowel","height"=>"open","backness"=>"front","rounding"=>"unrounded"},"b"=>{"sampa"=>"b","ipa"=>"b","wp"=>"Voiced_bilabial_plosive","desc"=>"voiced bilabial plosive","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"b_<"=>{"sampa"=>"b_<","ipa"=>"ɓ","wp"=>"Voiced_bilabial_implosive","desc"=>"voiced bilabial implosive","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"c"=>{"sampa"=>"c","ipa"=>"c","wp"=>"Voiceless_palatal_plosive","desc"=>"voiceless palatal plosive","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"d"=>{"sampa"=>"d","ipa"=>"d","wp"=>"Voiced_alveolar_plosive","desc"=>"voiced alveolar plosive","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"d`"=>{"sampa"=>"d`","ipa"=>"ɖ","wp"=>"Voiced_retroflex_plosive","desc"=>"voiced retroflex plosive","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"d_<"=>{"sampa"=>"d_<","ipa"=>"ɗ","wp"=>"Voiced_alveolar_implosive","desc"=>"voiced alveolar implosive","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"e"=>{"sampa"=>"e","ipa"=>"e","wp"=>"Close-mid_front_unrounded_vowel","desc"=>"close-mid front unrounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"front","rounding"=>"unrounded"},"f"=>{"sampa"=>"f","ipa"=>"f","wp"=>"Voiceless_labiodental_fricative","desc"=>"voiceless labiodental fricative","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"g"=>{"sampa"=>"g","ipa"=>"ɡ","wp"=>"Voiced_velar_plosive","desc"=>"voiced velar plosive","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"g_<"=>{"sampa"=>"g_<","ipa"=>"ɠ","wp"=>"Voiced_velar_implosive","desc"=>"voiced velar implosive","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"h"=>{"sampa"=>"h","ipa"=>"h","wp"=>"Voiceless_glottal_fricative","desc"=>"voiceless glottal fricative","type"=>"consonant","poa"=>"glottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"h\\"=>{"sampa"=>"h\\","ipa"=>"ɦ","wp"=>"Voiced_glottal_fricative","desc"=>"voiced glottal fricative","type"=>"consonant","poa"=>"glottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"i"=>{"sampa"=>"i","ipa"=>"i","wp"=>"Close_front_unrounded_vowel","desc"=>"close front unrounded vowel","type"=>"vowel","height"=>"close","backness"=>"front","rounding"=>"unrounded"},"j"=>{"sampa"=>"j","ipa"=>"j","wp"=>"Palatal_approximant","desc"=>"palatal approximant","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"j\\"=>{"sampa"=>"j\\","ipa"=>"ʝ","wp"=>"Voiced_palatal_fricative","desc"=>"voiced palatal fricative","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"k"=>{"sampa"=>"k","ipa"=>"k","wp"=>"Voiceless_velar_plosive","desc"=>"voiceless velar plosive","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"l"=>{"sampa"=>"l","ipa"=>"l","wp"=>"Alveolar_lateral_approximant","desc"=>"alveolar lateral approximant","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"l`"=>{"sampa"=>"l`","ipa"=>"ɭ","wp"=>"Retroflex_lateral_approximant","desc"=>"retroflex lateral approximant","type"=>"consonant","poa"=>"retroflex","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"l\\"=>{"sampa"=>"l\\","ipa"=>"ɺ","wp"=>"Alveolar_lateral_flap","desc"=>"alveolar lateral flap","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"flap","implosive"=>false,"voicing"=>"voiced"},"m"=>{"sampa"=>"m","ipa"=>"m","wp"=>"Bilabial_nasal","desc"=>"bilabial nasal","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"n"=>{"sampa"=>"n","ipa"=>"n","wp"=>"Alveolar_nasal","desc"=>"alveolar nasal","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"n`"=>{"sampa"=>"n`","ipa"=>"ɳ","wp"=>"Retroflex_nasal","desc"=>"retroflex nasal","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"o"=>{"sampa"=>"o","ipa"=>"o","wp"=>"Close-mid_back_rounded_vowel","desc"=>"close-mid back rounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"back","rounding"=>"rounded"},"p"=>{"sampa"=>"p","ipa"=>"p","wp"=>"Voiceless_bilabial_plosive","desc"=>"voiceless bilabial plosive","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"p\\"=>{"sampa"=>"p\\","ipa"=>"ɸ","wp"=>"Voiceless_bilabial_fricative","desc"=>"voiceless bilabial fricative","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"q"=>{"sampa"=>"q","ipa"=>"q","wp"=>"Voiceless_uvular_plosive","desc"=>"voiceless uvular plosive","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"r"=>{"sampa"=>"r","ipa"=>"r","wp"=>"Alveolar_trill","desc"=>"alveolar trill","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"trill","implosive"=>false,"voicing"=>"voiced"},"r`"=>{"sampa"=>"r`","ipa"=>"ɽ","wp"=>"Retroflex_flap","desc"=>"retroflex flap","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"flap","implosive"=>false,"voicing"=>"voiced"},"r\\"=>{"sampa"=>"r\\","ipa"=>"ɹ","wp"=>"Alveolar_approximant","desc"=>"alveolar approximant","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"r\\`"=>{"sampa"=>"r\\`","ipa"=>"ɻ","wp"=>"Retroflex_approximant","desc"=>"retroflex approximant","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"s"=>{"sampa"=>"s","ipa"=>"s","wp"=>"Voiceless_alveolar_fricative","desc"=>"voiceless alveolar fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"s`"=>{"sampa"=>"s`","ipa"=>"ʂ","wp"=>"Voiceless_retroflex_fricative","desc"=>"voiceless retroflex fricative","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"s\\"=>{"sampa"=>"s\\","ipa"=>"ɕ","wp"=>"Voiceless_alveolo-palatal_fricative","desc"=>"voiceless alveolo-palatal fricative","type"=>"consonant","poa"=>"alveolo-palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"t"=>{"sampa"=>"t","ipa"=>"t","wp"=>"Voiceless_alveolar_plosive","desc"=>"voiceless alveolar plosive","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"t`"=>{"sampa"=>"t`","ipa"=>"ʈ","wp"=>"Voiceless_retroflex_plosive","desc"=>"voiceless retroflex plosive","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiceless"},"u"=>{"sampa"=>"u","ipa"=>"u","wp"=>"Close_back_rounded_vowel","desc"=>"close back rounded vowel","type"=>"vowel","height"=>"close","backness"=>"back","rounding"=>"rounded"},"v"=>{"sampa"=>"v","ipa"=>"v","wp"=>"Voiced_labiodental_fricative","desc"=>"voiced labiodental fricative","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"v\\"=>{"sampa"=>"v\\","ipa"=>"ʋ","wp"=>"Labiodental_approximant","desc"=>"labiodental approximant","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"w"=>{"sampa"=>"w","ipa"=>"w","wp"=>"Labial-velar_approximant","desc"=>"labial-velar approximant","type"=>"consonant","rounding"=>"rounded","poa"=>"velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"x"=>{"sampa"=>"x","ipa"=>"x","wp"=>"Voiceless_velar_fricative","desc"=>"voiceless velar fricative","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"x\\"=>{"sampa"=>"x\\","ipa"=>"ɧ","wp"=>"Voiceless_palatal-velar_fricative","desc"=>"voiceless palatal-velar fricative","type"=>"consonant","poa"=>"palatal-velar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"y"=>{"sampa"=>"y","ipa"=>"y","wp"=>"Close_front_rounded_vowel","desc"=>"close front rounded vowel","type"=>"vowel","height"=>"close","backness"=>"front","rounding"=>"rounded"},"z"=>{"sampa"=>"z","ipa"=>"z","wp"=>"Voiced_alveolar_fricative","desc"=>"voiced alveolar fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"z`"=>{"sampa"=>"z`","ipa"=>"ʐ","wp"=>"Voiced_retroflex_fricative","desc"=>"voiced retroflex fricative","type"=>"consonant","poa"=>"retroflex","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"z\\"=>{"sampa"=>"z\\","ipa"=>"ʑ","wp"=>"Voiced_alveolo-palatal_fricative","desc"=>"voiced alveolo-palatal fricative","type"=>"consonant","poa"=>"alveolo-palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"A"=>{"sampa"=>"A","ipa"=>"ɑ","wp"=>"Open_back_unrounded_vowel","desc"=>"open back unrounded vowel","type"=>"vowel","height"=>"open","backness"=>"back","rounding"=>"unrounded"},"B"=>{"sampa"=>"B","ipa"=>"β","wp"=>"Voiced_bilabial_fricative","desc"=>"voiced bilabial fricative","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"B\\"=>{"sampa"=>"B\\","ipa"=>"ʙ","wp"=>"Bilabial_trill","desc"=>"bilabial trill","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"trill","implosive"=>false,"voicing"=>"voiced"},"C"=>{"sampa"=>"C","ipa"=>"ç","wp"=>"Voiceless_palatal_fricative","desc"=>"voiceless palatal fricative","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"D"=>{"sampa"=>"D","ipa"=>"ð","wp"=>"Voiced_dental_fricative","desc"=>"voiced dental fricative","type"=>"consonant","poa"=>"dental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"E"=>{"sampa"=>"E","ipa"=>"ɛ","wp"=>"Open-mid_front_unrounded_vowel","desc"=>"open-mid front unrounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"front","rounding"=>"unrounded"},"F"=>{"sampa"=>"F","ipa"=>"ɱ","wp"=>"Labiodental_nasal","desc"=>"labiodental nasal","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"G"=>{"sampa"=>"G","ipa"=>"ɣ","wp"=>"Voiced_velar_fricative","desc"=>"voiced velar fricative","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"G\\"=>{"sampa"=>"G\\","ipa"=>"ɢ","wp"=>"Voiced_uvular_plosive","desc"=>"voiced uvular plosive","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"G\\_<"=>{"sampa"=>"G\\_<","ipa"=>"ʛ","wp"=>"Voiced_uvular_implosive","desc"=>"voiced uvular implosive","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"H"=>{"sampa"=>"H","ipa"=>"ɥ","wp"=>"Labial-palatal_approximant","desc"=>"labial-palatal approximant","type"=>"consonant","rounding"=>"rounded","poa"=>"palatal","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"H\\"=>{"sampa"=>"H\\","ipa"=>"ʜ","wp"=>"Voiceless_epiglottal_fricative","desc"=>"voiceless epiglottal fricative","type"=>"consonant","poa"=>"epiglottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"I"=>{"sampa"=>"I","ipa"=>"ɪ","wp"=>"Near-close_near-front_unrounded_vowel","desc"=>"near-close near-front unrounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"near-front","rounding"=>"unrounded"},"I\\"=>{"sampa"=>"I\\","ipa"=>"ᵻ","wp"=>"Near-close_central_unrounded_vowel","desc"=>"near-close central unrounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"central","rounding"=>"unrounded"},"J"=>{"sampa"=>"J","ipa"=>"ɲ","wp"=>"Palatal_nasal","desc"=>"palatal nasal","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"J\\"=>{"sampa"=>"J\\","ipa"=>"ɟ","wp"=>"Voiced_palatal_plosive","desc"=>"voiced palatal plosive","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"J\\_<"=>{"sampa"=>"J\\_<","ipa"=>"ʄ","wp"=>"Voiced_palatal_implosive","desc"=>"voiced palatal implosive","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"plosive","implosive"=>true,"voicing"=>"voiced"},"K"=>{"sampa"=>"K","ipa"=>"ɬ","wp"=>"Voiceless_alveolar_lateral_fricative","desc"=>"voiceless alveolar lateral fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"K\\"=>{"sampa"=>"K\\","ipa"=>"ɮ","wp"=>"Voiced_alveolar_lateral_fricative","desc"=>"voiced alveolar lateral fricative","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"L"=>{"sampa"=>"L","ipa"=>"ʎ","wp"=>"Palatal_lateral_approximant","desc"=>"palatal lateral approximant","type"=>"consonant","poa"=>"palatal","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"L\\"=>{"sampa"=>"L\\","ipa"=>"ʟ","wp"=>"Velar_lateral_approximant","desc"=>"velar lateral approximant","type"=>"consonant","poa"=>"velar","lateral"=>true,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"M"=>{"sampa"=>"M","ipa"=>"ɯ","wp"=>"Close_back_unrounded_vowel","desc"=>"close back unrounded vowel","type"=>"vowel","height"=>"close","backness"=>"back","rounding"=>"unrounded"},"M\\"=>{"sampa"=>"M\\","ipa"=>"ɰ","wp"=>"Velar_approximant","desc"=>"velar approximant","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"N"=>{"sampa"=>"N","ipa"=>"ŋ","wp"=>"Velar_nasal","desc"=>"velar nasal","type"=>"consonant","poa"=>"velar","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"N\\"=>{"sampa"=>"N\\","ipa"=>"ɴ","wp"=>"Uvular_nasal","desc"=>"uvular nasal","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"nasal","implosive"=>false,"voicing"=>"voiced"},"O"=>{"sampa"=>"O","ipa"=>"ɔ","wp"=>"Open-mid_back_rounded_vowel","desc"=>"open-mid back rounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"back","rounding"=>"rounded"},"O\\"=>{"sampa"=>"O\\","ipa"=>"ʘ","wp"=>"Bilabial_click","desc"=>"bilabial click","type"=>"consonant","poa"=>"bilabial","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"P"=>{"sampa"=>"P","ipa"=>"ʋ","wp"=>"Labiodental_approximant","desc"=>"labiodental approximant","type"=>"consonant","poa"=>"labiodental","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"},"Q"=>{"sampa"=>"Q","ipa"=>"ɒ","wp"=>"Open_back_rounded_vowel","desc"=>"open back rounded vowel","type"=>"vowel","height"=>"open","backness"=>"back","rounding"=>"rounded"},"R"=>{"sampa"=>"R","ipa"=>"ʁ","wp"=>"Voiced_uvular_fricative","desc"=>"voiced uvular fricative","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"R\\"=>{"sampa"=>"R\\","ipa"=>"ʀ","wp"=>"Uvular_trill","desc"=>"uvular trill","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"trill","implosive"=>false,"voicing"=>"voiced"},"S"=>{"sampa"=>"S","ipa"=>"ʃ","wp"=>"Voiceless_postalveolar_fricative","desc"=>"voiceless postalveolar fricative","type"=>"consonant","poa"=>"postalveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"T"=>{"sampa"=>"T","ipa"=>"θ","wp"=>"Voiceless_dental_fricative","desc"=>"voiceless dental fricative","type"=>"consonant","poa"=>"dental","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"U"=>{"sampa"=>"U","ipa"=>"ʊ","wp"=>"Near-close_near-back_rounded_vowel","desc"=>"near-close near-back rounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"near-back","rounding"=>"rounded"},"U\\"=>{"sampa"=>"U\\","ipa"=>"ᵿ","wp"=>"Near-close_central_rounded_vowel","desc"=>"near-close central rounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"central","rounding"=>"rounded"},"V"=>{"sampa"=>"V","ipa"=>"ʌ","wp"=>"Open-mid_back_unrounded_vowel","desc"=>"open-mid back unrounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"back","rounding"=>"unrounded"},"W"=>{"sampa"=>"W","ipa"=>"ʍ","wp"=>"Voiceless_labial-velar_fricative","desc"=>"voiceless labial-velar fricative","type"=>"consonant","rounding"=>"rounded","poa"=>"velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiceless"},"X"=>{"sampa"=>"X","ipa"=>"χ","wp"=>"Voiceless_uvular_fricative","desc"=>"voiceless uvular fricative","type"=>"consonant","poa"=>"uvular","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"X\\"=>{"sampa"=>"X\\","ipa"=>"ħ","wp"=>"Voiceless_pharyngeal_fricative","desc"=>"voiceless pharyngeal fricative","type"=>"consonant","poa"=>"pharyngeal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiceless"},"Y"=>{"sampa"=>"Y","ipa"=>"ʏ","wp"=>"Near-close_near-front_rounded_vowel","desc"=>"near-close near-front rounded vowel","type"=>"vowel","height"=>"near-close","backness"=>"near-front","rounding"=>"rounded"},"Z"=>{"sampa"=>"Z","ipa"=>"ʒ","wp"=>"Voiced_postalveolar_fricative","desc"=>"voiced postalveolar fricative","type"=>"consonant","poa"=>"postalveolar","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"."=>{"sampa"=>".","ipa"=>".","desc"=>"syllable break","type"=>"nonsound","nonsound"=>"syllable"},"\""=>{"sampa"=>"\"","ipa"=>"ˈ","desc"=>"primary stress","wp"=>"Lexical_stress","type"=>"nonsound","nonsound"=>"primary"},"%"=>{"sampa"=>"%","ipa"=>"ˌ","wp"=>"Secondary_stress","desc"=>"secondary stress","type"=>"nonsound","nonsound"=>"secondary"},"'"=>{"sampa"=>"'","ipa"=>"ʲ","wp"=>"Palatalization","desc"=>"palatalized","type"=>"mod","menu"=>"back-coarticulation","value"=>"palatalized"},"=>"=>{"sampa"=>"=>","ipa"=>"ː","desc"=>"long","type"=>"mod","menu"=>"length","value"=>"long"},"=>\\"=>{"sampa"=>"=>\\","ipa"=>"ˑ","desc"=>"half long","type"=>"mod","menu"=>"length","value"=>"half"},"@"=>{"sampa"=>"@","ipa"=>"ə","wp"=>"Schwa","type"=>"vowel","rounding"=>"unrounded","height"=>"mid","backness"=>"central","desc"=>"schwa"},"@\\"=>{"sampa"=>"@\\","ipa"=>"ɘ","wp"=>"Close-mid_central_unrounded_vowel","desc"=>"close-mid central unrounded vowel","type"=>"vowel","height"=>"close-mid","backness"=>"central","rounding"=>"unrounded"},"{"=>{"sampa"=>"{","ipa"=>"æ","wp"=>"Near-open_front_unrounded_vowel","desc"=>"near-open front unrounded vowel","type"=>"vowel","height"=>"near-open","backness"=>"front","rounding"=>"unrounded"},"}"=>{"sampa"=>"}","ipa"=>"ʉ","wp"=>"Close_central_rounded_vowel","desc"=>"close central rounded vowel","type"=>"vowel","height"=>"close","backness"=>"central","rounding"=>"rounded"},"3\\"=>{"sampa"=>"3\\","ipa"=>"ɞ","wp"=>"Open-mid_central_rounded_vowel","desc"=>"open-mid central rounded vowel","type"=>"vowel","height"=>"open-mid","backness"=>"central","rounding"=>"rounded"},"&"=>{"sampa"=>"&","ipa"=>"ɶ","wp"=>"Open_front_rounded_vowel","desc"=>"open front rounded vowel","type"=>"vowel","height"=>"open","backness"=>"front","rounding"=>"rounded"},"?"=>{"sampa"=>"?","ipa"=>"ʔ","wp"=>"Glottal_stop","type"=>"consonant","poa"=>"glottal","moa"=>"plosive","voicing"=>"voiceless","implosive"=>false,"desc"=>"glottal stop"},"?\\"=>{"sampa"=>"?\\","ipa"=>"ʕ","wp"=>"Voiced_pharyngeal_fricative","desc"=>"voiced pharyngeal fricative","type"=>"consonant","poa"=>"pharyngeal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},"<\\"=>{"sampa"=>"<\\","ipa"=>"ʢ","wp"=>"Voiced_epiglottal_fricative","desc"=>"voiced epiglottal fricative","type"=>"consonant","poa"=>"epiglottal","lateral"=>false,"moa"=>"fricative","implosive"=>false,"voicing"=>"voiced"},">\\"=>{"sampa"=>">\\","ipa"=>"ʡ","wp"=>"Epiglottal_plosive","desc"=>"epiglottal plosive","type"=>"consonant","poa"=>"epiglottal","lateral"=>false,"moa"=>"plosive","implosive"=>false,"voicing"=>"voiced"},"^"=>{"sampa"=>"^","ipa"=>"ꜛ","wp"=>"Upstep_(phonetics)","desc"=>"upstep","type"=>"nonsound","nonsound"=>"upstep"},"!"=>{"sampa"=>"!","ipa"=>"ꜜ","wp"=>"Downstep_(phonetics)","desc"=>"downstep","type"=>"nonsound","nonsound"=>"downstep"},"!\\"=>{"sampa"=>"!\\","ipa"=>"ǃ","wp"=>"Postalveolar_click","desc"=>"postalveolar click","type"=>"consonant","poa"=>"postalveolar","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"|"=>{"sampa"=>"|","ipa"=>"|","desc"=>"minor (foot) group","type"=>"nonsound","nonsound"=>"minor"},"|\\"=>{"sampa"=>"|\\","ipa"=>"ǀ","wp"=>"Dental_click","desc"=>"dental click","type"=>"consonant","poa"=>"dental","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"||"=>{"sampa"=>"||","ipa"=>"‖","desc"=>"major (intonation) group","type"=>"nonsound","nonsound"=>"major"},"|\\|\\"=>{"sampa"=>"|\\|\\","ipa"=>"ǁ","wp"=>"Alveolar_lateral_click","desc"=>"alveolar lateral click","type"=>"consonant","poa"=>"alveolar","lateral"=>true,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"=\\"=>{"sampa"=>"=\\","ipa"=>"ǂ","wp"=>"Palatal_click","desc"=>"palatal click","type"=>"consonant","poa"=>"palatal","lateral"=>false,"moa"=>"click","implosive"=>false,"voicing"=>"voiceless"},"-\\"=>{"sampa"=>"-\\","ipa"=>"‿","wp"=>"Liaison_(linguistics)","desc"=>"linking mark","type"=>"nonsound","nonsound"=>"linking"},"_\""=>{"sampa"=>"_\"","ipa"=>" ̈","wp"=>"Centralization_(phonetics)","desc"=>"centralized","type"=>"mod","menu"=>"centralization","value"=>"centralized"},"_+"=>{"sampa"=>"_+","ipa"=>" ̟","wp"=>"Advanced_(phonetics)","desc"=>"advanced","type"=>"mod","menu"=>"advancedness","value"=>"advanced"},"_-"=>{"sampa"=>"_-","ipa"=>" ̠","wp"=>"Retracted_(phonetics)","desc"=>"retracted","type"=>"mod","menu"=>"advancedness","value"=>"retracted"},"_/"=>{"sampa"=>"_/","ipa"=>" ̌","desc"=>"rising tone","type"=>"mod","menu"=>"tone","value"=>"rising"},"_0"=>{"sampa"=>"_0","ipa"=>" ̥","wp"=>"Voiceless_consonant","desc"=>"voiceless","type"=>"mod","menu"=>"voicing","value"=>"voiceless"},"_<"=>{"sampa"=>"_<","ipa"=>"(implosive)","wp"=>"Implosive_consonant","desc"=>"implosive","type"=>"mod","menu"=>"ejective","value"=>"implosive"},"="=>{"sampa"=>"=","ipa"=>" ̩","desc"=>"syllabic","type"=>"mod","checkbox"=>"syllabic","value"=>true},"_>"=>{"sampa"=>"_>","ipa"=>"ʼ","wp"=>"Ejective_consonant","desc"=>"ejective","type"=>"mod","menu"=>"ejective","value"=>"ejective"},"_?\\"=>{"sampa"=>"_?\\","ipa"=>"ˤ","wp"=>"Pharyngealisation","desc"=>"pharyngealized","type"=>"mod","menu"=>"back-coarticulation","value"=>"pharyngealized"},"_\\"=>{"sampa"=>"_\\","ipa"=>" ̂","desc"=>"falling tone","type"=>"mod","menu"=>"tone","value"=>"falling"},"_^"=>{"sampa"=>"_^","ipa"=>" ̯","desc"=>"non-syllabic","type"=>"mod","checkbox"=>"syllabic","value"=>false},"_}"=>{"sampa"=>"_}","ipa"=>" ̚","wp"=>"No_audible_release","type"=>"mod","menu"=>"unreleased","value"=>"unreleased","desc"=>"no audible release"},"`"=>{"sampa"=>"`","ipa"=>"˞","wp"=>"R-colored_vowel","desc"=>"rhotacization","type"=>"mod","checkbox"=>"rhotacization","value"=>true},"~"=>{"sampa"=>"~","ipa"=>" ̃","wp"=>"Nasalization","desc"=>"nasalization","type"=>"mod","checkbox"=>"nasalization","value"=>true},"_A"=>{"sampa"=>"_A","ipa"=>" ̘","wp"=>"Advanced_tongue_root","type"=>"mod","menu"=>"atr","value"=>"advanced","desc"=>"advanced tongue root"},"_a"=>{"sampa"=>"_a","ipa"=>" ̺","wp"=>"Apical_consonant","desc"=>"apical","type"=>"mod","menu"=>"apical","value"=>"apical"},"_B"=>{"sampa"=>"_B","ipa"=>" ̏","desc"=>"extra low tone","type"=>"mod","menu"=>"tone","value"=>"extra-low"},"_B_L"=>{"sampa"=>"_B_L","ipa"=>" ᷅","desc"=>"low rising tone","type"=>"mod","menu"=>"tone","value"=>"low-rising"},"_c"=>{"sampa"=>"_c","ipa"=>" ̜","desc"=>"less rounded","type"=>"mod","menu"=>"rounding","value"=>"less"},"_d"=>{"sampa"=>"_d","ipa"=>" ̪","wp"=>"Dental_consonant","desc"=>"dental","type"=>"mod","menu"=>"poa","value"=>"dental"},"_e"=>{"sampa"=>"_e","ipa"=>" ̴","desc"=>"velarized or pharyngealized","type"=>"mod","menu"=>"back-coarticulation","value"=>"velarized"},"<F>"=>{"sampa"=>"<F>","ipa"=>"↘","wp"=>"Intonation_(linguistics)","desc"=>"global fall","type"=>"nonsound","nonsound"=>"fall"},"_F"=>{"sampa"=>"_F","ipa"=>" ̂","desc"=>"falling tone","type"=>"mod","menu"=>"tone","value"=>"falling"},"_G"=>{"sampa"=>"_G","ipa"=>"ˠ","wp"=>"Velarization","desc"=>"velarized","type"=>"mod","menu"=>"back-coarticulation","value"=>"velarized"},"_H"=>{"sampa"=>"_H","ipa"=>" ́","desc"=>"high tone","type"=>"mod","menu"=>"tone","value"=>"high"},"_H_T"=>{"sampa"=>"_H_T","ipa"=>" ᷄","desc"=>"high rising tone","type"=>"mod","menu"=>"tone","value"=>"high-rising"},"_h"=>{"sampa"=>"_h","ipa"=>"ʰ","wp"=>"Aspiration_(phonetics)","desc"=>"aspirated","type"=>"mod","checkbox"=>"aspirated","value"=>true},"_j"=>{"sampa"=>"_j","ipa"=>"ʲ","wp"=>"Palatalization","desc"=>"palatalized","type"=>"mod","menu"=>"back-coarticulation","value"=>"palatalized"},"_k"=>{"sampa"=>"_k","ipa"=>" ̰","wp"=>"Creaky_voice","desc"=>"creaky voice","type"=>"mod","menu"=>"voicing","value"=>"creaky"},"_L"=>{"sampa"=>"_L","ipa"=>" ̀","desc"=>"low tone","type"=>"mod","menu"=>"tone","value"=>"low"},"_l"=>{"sampa"=>"_l","ipa"=>"ˡ","wp"=>"Lateral_release_(phonetics)","desc"=>"lateral release","type"=>"mod","checkbox"=>"lateral","value"=>true},"_M"=>{"sampa"=>"_M","ipa"=>" ̄","desc"=>"mid tone","type"=>"mod","menu"=>"tone","value"=>"mid"},"_m"=>{"sampa"=>"_m","ipa"=>" ̻","wp"=>"Laminal_consonant","desc"=>"laminal","type"=>"mod","menu"=>"apical","value"=>"laminal"},"_N"=>{"sampa"=>"_N","ipa"=>" ̼","wp"=>"Linguolabial_consonant","desc"=>"linguolabial","type"=>"mod","menu"=>"poa","value"=>"linguolabial"},"_n"=>{"sampa"=>"_n","ipa"=>"ⁿ","wp"=>"Nasal_release","type"=>"mod","menu"=>"moa","value"=>"prestopped","desc"=>"nasal release"},"_O"=>{"sampa"=>"_O","ipa"=>" ̹","wp"=>"Roundedness","desc"=>"more rounded","type"=>"mod","menu"=>"rounding","value"=>"more"},"_o"=>{"sampa"=>"_o","ipa"=>" ̞","wp"=>"Relative_articulation#Raised_and_lowered","desc"=>"lowered","type"=>"mod","menu"=>"raisedness","value"=>"lowered"},"_q"=>{"sampa"=>"_q","ipa"=>" ̙","wp"=>"Retracted_tongue_root","type"=>"mod","menu"=>"atr","value"=>"retracted","desc"=>"retracted tongue root"},"<R>"=>{"sampa"=>"<R>","ipa"=>"↗","wp"=>"Intonation_(linguistics)","desc"=>"global rise","type"=>"nonsound","nonsound"=>"rise"},"_R"=>{"sampa"=>"_R","ipa"=>" ̌","desc"=>"rising tone","type"=>"mod","menu"=>"tone","value"=>"rising"},"_R_F"=>{"sampa"=>"_R_F","ipa"=>" ᷈","desc"=>"rising falling tone","type"=>"mod","menu"=>"tone","value"=>"rising-falling"},"_r"=>{"sampa"=>"_r","ipa"=>" ̝","wp"=>"Relative_articulation#Raised_and_lowered","desc"=>"raised","type"=>"mod","menu"=>"raisedness","value"=>"raised"},"_T"=>{"sampa"=>"_T","ipa"=>" ̋","desc"=>"extra high tone","type"=>"mod","menu"=>"tone","value"=>"extra-high"},"_t"=>{"sampa"=>"_t","ipa"=>" ̤","wp"=>"Breathy_voice","desc"=>"breathy voice","type"=>"mod","menu"=>"voicing","value"=>"breathy"},"_v"=>{"sampa"=>"_v","ipa"=>" ̬","wp"=>"Voiced_consonant","desc"=>"voiced","type"=>"mod","menu"=>"voicing","value"=>"voiced"},"_w"=>{"sampa"=>"_w","ipa"=>"ʷ","wp"=>"Labialisation","desc"=>"labialized","type"=>"mod","menu"=>"rounding","value"=>"rounded"},"_X"=>{"sampa"=>"_X","ipa"=>" ̆","desc"=>"extra-short","type"=>"mod","menu"=>"length","value"=>"extra-short"},"_x"=>{"sampa"=>"_x","ipa"=>" ̽","desc"=>"mid-centralized","type"=>"mod","menu"=>"centralization","value"=>"mid-centralized"},"_="=>{"sampa"=>"=","ipa"=>" ̩","desc"=>"syllabic","type"=>"mod","checkbox"=>"syllabic","value"=>true},"_~"=>{"sampa"=>"~","ipa"=>" ̃","wp"=>"Nasalization","desc"=>"nasalization","type"=>"mod","checkbox"=>"nasalization","value"=>true},"_"=>{"sampa"=>"_","ipa"=>"","desc"=>"…","type"=>"mod","isNull"=>true});
our %SOUND_DESC = (
	'1' => 'See <a href="search.cgi?q=¹"><sup>1</sup></a> or <a href="search.cgi?q=₁"><sub>1</sub></a> instead.',
	'2' => 'See <a href="search.cgi?q=²"><sup>2</sup></a> or <a href="search.cgi?q=₂"><sub>2</sub></a> instead.',
	'3' => 'See <a href="search.cgi?q=³"><sup>3</sup></a> or <a href="search.cgi?q=₃"><sub>3</sub></a> instead.',
	'4' => 'See <a href="search.cgi?q=⁴"><sup>4</sup></a> or <a href="search.cgi?q=₄"><sub>4</sub></a> instead.',
	'5' => 'See <a href="search.cgi?q=⁵"><sup>5</sup></a> instead.',
	#'plain 1' => 'See <a href="search.cgi?q=plain+¹">plain <sup>1</sup></a> or <a href="search.cgi?q=plain+₁">plain <sub>1</sub></a> instead.',
	#'plain 2' => 'See <a href="search.cgi?q=plain+²">plain <sup>2</sup></a> or <a href="search.cgi?q=plain+₂">plain <sub>2</sub></a> instead.',
	#'plain 3' => 'See <a href="search.cgi?q=plain+³">plain <sup>3</sup></a> or <a href="search.cgi?q=plain+₃">plain <sub>3</sub></a> instead.',
	'plain 1' => 'See <a href="search.cgi?q=¹"><sup>1</sup></a> or <a href="search.cgi?q=₁"><sub>1</sub></a> instead.',
	'plain 2' => 'See <a href="search.cgi?q=²"><sup>2</sup></a> or <a href="search.cgi?q=₂"><sub>2</sub></a> instead.',
	'plain 3' => 'See <a href="search.cgi?q=³"><sup>3</sup></a> or <a href="search.cgi?q=₃"><sub>3</sub></a> instead.',
	'1ː' => 'See <a href="search.cgi?q=₁ː"><sub>1</sub>ː</a> instead.',
	'2ː' => 'See <a href="search.cgi?q=₂ː"><sub>2</sub>ː</a> instead.',
	'3ː' => 'See <a href="search.cgi?q=³ː"><sup>3</sup>ː</a> instead.',
	'1ˤ' => 'See <a href="search.cgi?q=₁ˤ"><sub>1</sub>ˤ</a> instead.',
	'2ˤ' => 'See <a href="search.cgi?q=₂ˤ"><sub>2</sub>ˤ</a> instead.',
);

sub ueq {
	my ($a, $b) = @_;
	if(!defined($a) && !defined($b)) {
		return 1;
	}
	if(defined($a) && defined($b) && $a eq $b) {
		return 1;
	}
	return 0;
}

sub escapeHTML {
	local $_ = $_[0];
	s{&}{&amp;}g;
	s{<}{&lt;}g;
	s{>}{&gt;}g;
	s{"}{&quot;}g;
	return $_;
}
my %SUP_SUB = (
	'<sup>0</sup>' => '⁰',
	'<sup>1</sup>' => '¹',
	'<sup>2</sup>' => '²',
	'<sup>3</sup>' => '³',
	'<sup>4</sup>' => '⁴',
	'<sup>5</sup>' => '⁵',
	'<sub>0</sub>' => '₀',
	'<sub>1</sub>' => '₁',
	'<sub>2</sub>' => '₂',
	'<sub>3</sub>' => '₃',
	'<sub>4</sub>' => '₄',
	'<sub>5</sub>' => '₅',
	'⁰' => '<sup>0</sup>',
	'¹' => '<sup>1</sup>',
	'²' => '<sup>2</sup>',
	'³' => '<sup>3</sup>',
	'⁴' => '<sup>4</sup>',
	'⁵' => '<sup>5</sup>',
	'₀' => '<sub>0</sub>',
	'₁' => '<sub>1</sub>',
	'₂' => '<sub>2</sub>',
	'₃' => '<sub>3</sub>',
	'₄' => '<sub>4</sub>',
	'₅' => '<sub>5</sub>',
);
sub format_title { my ($sound) = @_;
	$sound =~ s{(<su([pb])>[0-5]</su\2>)}{$SUP_SUB{$1}}eg;
	$sound =~ s{<[^>]+>}{}g;
	return $sound;
}
sub from_html { my ($sound) = @_;
	$sound =~ s{(<su([pb])>[0-5]</su\2>)}{$SUP_SUB{$1}}eg;
	#$sound =~ s{\[[^\]]+\]}{}g;
	$sound =~ s{<[^>]+>}{}g;
	$sound =~ s{&amp;}{&}g;
	return $sound;
};

# semi-based on escape from CGI::Util
sub escape {
	local $_ = $_[0];
	utf8::encode($_);
	s{[^a-zA-Z0-9_.~-]}{sprintf('%%%02x', ord($&))}ge;
	return $_;
}

sub hlchar {
	my ($line, $pos) = @_;
	substr($line, $pos, 1) = "\e[41m".substr($line, $pos, 1)."\e[0m";
	return $line;
}

sub make_id {
	my ($text, $ids_seen) = @_;
	local $_ = $text;
	s{\s|→}{-}g;
	s{“.*$}{};
	s{<[^>]*>}{}g;
	s{[()\[\]{}<>&]}{}g;
	s{^-+}{};
	s{-+$}{};
	s{-+}{-}g;
	$_ = NFC($_);
	if($ids_seen->{$_}) {
		my $n = 2;
		while($ids_seen->{$_.'_'.$n}) {
			$n++;
		}
		$_ .= '_'.$n;
	}
	$ids_seen->{$_} = 1;
	return $_;
}

sub make_C_id {
	my ($text, $ids_seen) = @_;
	$text =~ s{ /.*$}{};
	$text =~ s{\s*→.*$}{};
	$text =~ s{[‹›]}{}g;
	$text =~ s{«([^«»]+)»?}{}g;
	return make_id($text, $ids_seen);
}

sub make_S_id {
	my ($text, $ids_seen) = @_;
	$text =~ s{^\d+(\.\d+)*}{};
	$text =~ s{^.*\bto\b}{};
	return make_id($text, $ids_seen);
}

sub open_data {
	open my $fh, '<:utf8', $DATA_FILE or die "Can't open data file $!";
	return $fh;
}

sub read_data {
	my $fh = open_data;
	my %callbacks = @_;
	my %seen;
	my %context;
	my @seeks;
	@seeks = @{$callbacks{seeks}} if $callbacks{seeks};
	my $i = 0;
	while(<$fh>) {
		if(/^S ((\d+(\.\d+)*) .*)$/) {
			my ($name, $num) = ($1, $2);
			my $is_main = !$3;
			my $id = make_S_id($name, \%seen);
			@context{'prev_name','prev_num','prev_sid'} = @context{'name','num','sid'};
			@context{'name','num','sid'} = ($name,$num,$id);
			if($is_main) {
				@context{'main_name','main_num','main_sid'} = ($name,$num,$id);
				$context{is_main} = 1;
				$callbacks{main_section}(%context) if defined($callbacks{main_section});
			} else {
				$context{is_main} = 0;
				$callbacks{subsection}(%context) if defined($callbacks{subsection});
			}
			$callbacks{section}(%context) if defined($callbacks{section});
		} elsif(/^s ([^\cA]*)\cA((\d+(\.\d+)*) .*)$/) {
			my ($id, $name, $num) = ($1, $2, $3);
			my $is_main = !$4;
			@context{'prev_name','prev_num','prev_sid'} = @context{'name','num','sid'};
			@context{'name','num','sid'} = ($name,$num,$id);
			if($is_main) {
				@context{'main_name','main_num','main_sid'} = ($name,$num,$id);
				$context{is_main} = 1;
				$callbacks{main_section}(%context) if defined($callbacks{main_section});
			} else {
				$context{is_main} = 0;
				$callbacks{subsection}(%context) if defined($callbacks{subsection});
			}
			$callbacks{section}(%context) if defined($callbacks{section});
		} elsif(/^T (.*)$/) {
			$callbacks{text}($1, %context) if defined($callbacks{text});
		} elsif(/^</) {
			$callbacks{table_start}(%context) if defined($callbacks{table_start});
			$context{row} = 0;
		} elsif(/^R (.*)$/) {
			$callbacks{table_row}($1, %context) if defined($callbacks{table_row});
			$context{row}++;
		} elsif(/^>/) {
			$callbacks{table_end}(%context) if defined($callbacks{table_end});
		} elsif(/^C (.*)$/) {
			my $c = $1;
			my $id = make_C_id($context{sid} . '-' . $c, \%seen);
			$callbacks{change}($c, $id, %context) if defined($callbacks{change});
		} elsif(/^c (.*)$/) {
			my $c = $1;
			$callbacks{pchange}($c, %context) if defined($callbacks{pchange});
		} elsif(!/^#/) {
			warn "Unrecognized line $_";
		}
		if(@seeks) {
			last if($i > $#seeks);
			seek $fh, $seeks[$i], 0;
			$i++;
		}
		$context{byte} = tell $fh;
	}
	close $fh;
}

sub make_html {
	local $_;
	my @sections;
	my %seen;
	
	
	my $section_name;
	my $section_id;
	my $section_main_number;
	my $r;
	
	read_data(
		section => sub {
			my %c = @_;
			if($c{prev_name}) {
				$r .= "</section>\n";
				$sections[$#sections]{body} = NFC($r);
			}
			$r = qq(<section class="showtarget" id="$c{sid}">\n<h2>$c{name}</h2>\n);
			push @sections, {name=>$c{name}, id=>$c{sid}, main_number=>$c{main_number}};
		},
		text => sub {
			$r .= $_[0] . "\n";
		},
		table_start => sub {
			$r .= "<table>\n";
		},
		table_row => sub {
			my $data = $_[0];
			$data =~ s{\t}{<td>}g;
			$r .= "<tr><td>$data\n";
		},
		table_end => sub {
			$r .= "</table>\n";
		},
		change => sub {
			my ($c, $id) = @_[0,1];
			$c =~ s{(‹|^)[^‹›]+›}{}g;
			$c =~ s{[«»]}{}g;
			$r .= qq{<p class="schg" id="$id">} . $c . "\n";
		}
	);
	$r .= "</section>\n";
	$sections[$#sections]{body} = NFC($r);
	if(wantarray) {
		return @sections;
	} else {
		return [@sections];
	}
}

sub make_toc {
	my ($which, $fname, @sections) = @_;
	my $result = "<ol>";
	my $level = 1;
	for my $s (@sections) {
		next if $which && $s->{main_number} != $which;
		$s->{name} =~ m{^([0-9.]+)\s+(.*)$};
		my ($num, $name) = ($1, $2);
		my @num = split(/\./, $num);
		while($level < @num) {
			$result .= "<ol>";
			$level++;
		}
		while($level > @num) {
			$result .= "</ol>";
			$level--;
		}
		if(@num == 1) {
			$result .= qq(\n<li value="$num[0]">);
		} else {
			$result .= "\n<li>";
		}
		$result .= qq(<a href="$fname#$s->{id}">$name</a>);
	}
	while($level > 0) {
		$result .= "</ol>";
		$level--;
	}
	return NFC($result);
}

sub make_html_of_sections {
	my @which_sections = @_;
	my @sections = make_html();
	my $result = '';
	for my $ss (@sections) {
		$ss->{name} =~ /^(\d+)/;
		my $number = $1;
		my $include = !@which_sections;
		for my $s (@which_sections) {
			if($number == $s) {
				$include = 1;
				last;
			}
		}
		if($include) {
			$result .= $ss->{body};
		}
	}
	return $result;
}

sub sound_changes_to_text {
	return join('', map {$_->{text}} @_);
}

# this gives duplicate results, but it should be good enough for now...
sub parse_braces {
	local $_ = $_[0];
	$_ = '' if !defined($_); # FIXME fix this for real...
	s/\(([^)}]+)\)/{,$1}/g;
	if($_ =~ m(\{([^{}]+)\})) {
		my ($before, $during, $after) = ($`, $1, $');
		my @result;
		my @during = split /,/, $during;
		for my $d (@during) {
			push @result, parse_braces($before . $d . $after);
		}
		return @result;
	}
	return $_;
}

sub parse_sound_change {
	local $_ = $_[0];
	my $original = $_;
	s{^[–—]\s*}{};
	s{[‹›]}{}g;
	s{«([^«»]+)»?}{}g;
	while(s{(\[[^\]]*) }{$1 }g) {} # replace spaces in brackets with nbsp
	my ($context, $negcontext, $text) = ('', '');
	my $has_context = 0;
	if(m{ / }) {
		($_, $text) = ($`, $');
		$has_context = 1;
		$text =~ m{^([^ \t]*_[^ \t]*)?(?:\s*! ([^ \t]*_[^ \t]*))?} and
			($context, $negcontext, $text) = ($1, $2, $');
	}
	my @stages_str = split /\s*→\s*/, $_;
	my (@from, @to, @context);
	my $nwords;
	warn "No → in sound change $original" if @stages_str <= 1;
	for my $i (0..$#stages_str) {
		my ($str) = ($stages_str[$i]);
		$str =~ s{^\s*|\s*$}{}g;
		$str =~ s{^\)|\($}{}g;
		$str =~ s{^\(}{} unless $str =~ /\)/;
		$str =~ s{\??\)$}{} unless $str =~ /\(/;
		my @w = split / +/, $str;
		if($i == 0) {
			$nwords = @w;
		} else {
			warn "Mismatched sound changes in $original (i=$i; nwords=$nwords)"
				if $REPORT_MISMATCHED && ($i==$#stages_str ? @w < $nwords : @w != $nwords);
			$#w = $nwords-1;
		}
		my @stage = map {[parse_braces($_)]} @w;
		push @from, @stage unless $i == $#stages_str;
		push @to, @stage unless $i == 0;
		#if($i == $#stages_str-1 && $to =~ /([^ ]+ +){$nwords}/) {
		#$text = $';
		#}
	}
	push @context, [parse_braces($context)] if $context;
	push @context, [parse_braces($negcontext)] if $negcontext;
	return (from => [@from], to => [@to], context => [@context]);
}

sub parse_sound_change_processed {
	my $line = $_[0];
	my ($id, $html, $from, $to, $context) = split /\cA/, $line;
	return (id=>$id, html=>$html,
		from => [map {[split /\cC/, $_]} split /\cB/, $from],
		to => [map {[split /\cC/, $_]} split /\cB/, $to],
		context => [map {[split /\cC/, $_]} split /\cB/, $context]);
}

sub sounds {
	my %change = @_;
	return ((map {@$_} @{$change{from}}),
	        (map {@$_} @{$change{to}}),
	        (map {@$_} @{$change{context}}));
}

sub normalize_sound {
	local $_ = $_[0];
	$_ = NFD($_);
	s{\xa0}{\x20}g;
	s{ç}{ç}g; # recompose
	tr{g‘’'!ı͡}{ɡʼʼʼǃi͜};
	s{(\[[+\-]\s*[^\[\] ]+)\s([+-])}{$1][$2}g;
	s{(\[[+\-])\s+}{$1}g;
	s{\[([+\-])nas\]}{[$1nasal]}g;
	s{\[([+\-])rounded\]}{[$1round]}g;
	s{\[([+\-])voiced\]}{[$1voice]}g;
	return $_;
}

sub separate_sounds {
	local $_ = normalize_sound($_[0]);
	my @result;
	#while(/\G(?:([’ʼ][mnɲptdɟkgɡ]|ᵐ[pb]|ⁿ[tdǀ]|ᵑ[kɡg])|([^ʰ-\x{036f}]))?((?:[͜͡].|[ʰ-\x{036f}])*)/g) {
	while(/\G(?:([’ʼ][mnɲptdɟkgɡ◌]|ᵐ[pb◌]|ⁿ[tdǀ◌]|ᵑ[kɡg◌])|([^ʰ-\x{036f}\[¹²³⁴⁵₀-₉]))?((?:[͜͡][qɢ]|[ʰ-\x{036f}¹²³⁴⁵₀-₉]|\[[^\]]*\])*)/g) {
		my ($pre, $main, $mod, $full) = ($1, $2, $3, $&);
		next if $full eq '';
		if($pre) {
			$main = substr $pre,1;
			$pre = substr $pre,0,1;
		} else {
			$pre = '';
			$main //= '';
		}
		$mod //= '';
		#push @result, {pre=>$pre, main=>$main, mod=>$mod, full=>$full, vowel=>$main && ($main eq 'B' || $main eq 'E' || $main eq 'V' || ($IPA{$main} && $IPA{$main}{type} && $IPA{$main}{type})) eq 'vowel'}; # TODO
		push @result, {pre=>$pre, main=>$main, mod=>$mod, full=>$full, vowel=>$main && $IPA{$main} && $IPA{$main}{type} && $IPA{$main}{type} eq 'vowel'};
	}
	return @result;
}

sub parse_query_sound {
	local $_ = $_[0];
	my $plain = 0;
	if(/^plain\s+/) {
		s///;
		$plain = 1;
	}
	my $result = [separate_sounds($_)];
	if($plain) {
		for my $r (@{$result}) {
			$r->{plain} = 1;
		}
	}
	return $result;
}

sub parse_query {
	my $query = $_[0];
	if($query =~ /^\s*(?:(?:<|>|<>)\s*)?$/) {
		return {from=>[], to=>[], oneway=>1, raw=>''};
	}
	if($query =~ /^(.*?)\s*(<|>|<>)\s*(.*)$/) {
		my ($first, $op, $last) = ($1, $2, $3);
		($first, $last) = (parse_query_sound($first), parse_query_sound($last));
		if($op eq '>') {
			return {from=>$first, to=>$last, oneway=>1, raw=>$_[0]};
		} elsif($op eq '<') {
			return {from=>$last, to=>$first, oneway=>1, raw=>$_[0]};
		} else {
			return {from=>$first, to=>$last, oneway=>0, raw=>$_[0]};
		}
	} else {
		return {from=>parse_query_sound($query), to=>[], oneway=>0, raw=>$_[0]};
	}
}

sub format_sound {
	my $sound = $_[0];
	if(@{$sound} == 0) {return '';}
	return '/' . circle_diacritics(escapeHTML(join '', map {$_->{full}} @{$sound})) . '/';
}

sub format_query {
	my ($dir, $query) = @_;
	my ($from, $to) = (format_sound($query->{from}), format_sound($query->{to}));
	if($dir eq 'context') {
		return "with context involving $from";
	}
	if($dir eq 'either' && !$query->{oneway}) {
		if($from && $to) {
			return "involving $from and $to";
		} elsif($from) {
			return "involving $from";
		} elsif($to) {
			return "involving $to";
		} else {
			return "all";
		}
	} elsif($dir eq 'reverse') {
		($from, $to) = ($to, $from);
	}
	if($from && $to) {
		return "from $from to $to";
	} elsif($from) {
		return "from $from";
	} elsif($to) {
		return "to $to";
	} else {
		return "all";
	}
}

sub match_change {
	my ($query, %change) = @_;
	my ($from, $to, $context) = (0, 0);
	for my $i (0..$#{$change{from}}) {
		my ($cf, $ct) = (0, 0);
		for my $f (@{$change{from}[$i]}) {
			if(match_sound($query->{from}, $f)) {
				$cf = 1;
			}
			if(!$query->{oneway} && match_sound($query->{to}, $f)) {
				$ct = 1;
			}
		}
		for my $t (@{$change{to}[$i]}) {
			if($cf && match_sound($query->{to}, $t)) {
				$from = 1;
			}
			if($ct && match_sound($query->{from}, $t)) {
				$to = 1;
			}
		}
	}
	for my $c (@{$change{context}}) {
		for my $a (@{$c}) {
			if(match_sound($query->{from}, $a) && !@{$query->{to}} && !$query->{oneway}) {
				$context = 1;
			}
		}
	}
	return ($from ? 'f' : '') . ($to ? 't' : '') . ($context ? 'c' : '');
}

sub match_sound_part { my ($query, $sound) = @_;
	if($query->{plain}) {
		return $sound->{full} eq $query->{full};
	} else {
		return 0 unless index($sound->{pre}, $query->{pre}) >= 0;
		return 0 unless index($sound->{main}, $query->{main}) >= 0 ||
		                $query->{main} eq '◌';
		return 0 unless index($sound->{mod}, $query->{mod}) >= 0 ||
		                ($query->{main} eq '' && index($sound->{pre}, $query->{mod}) >= 0);
		return 1;
	}
}

sub match_sound {
	my ($query, $sound) = @_;
	return 1 if @{$query} == 0;
	$sound = from_html($sound);
	#warn "Not in NFD: $sound" unless checkNFD($sound);
	my @sound = separate_sounds($sound);
	
	my $any_match = 0;
	for my $i (0..(@sound-@{$query})) {
		my $match = 1;
		my $matched = '';
		my $is_vowel = 1;
		for my $j (0..$#{$query}) {
			if(!match_sound_part($query->[$j], $sound[$i+$j])) {
				$match = 0;
				last;
			}
			$matched .= $sound[$i+$j]{full};
			$is_vowel &&= $sound[$i+$j]{vowel};
		}
		next unless $match;
		$any_match = 1;
		$query->[0]{variants}{$matched} = 1;
		if(@sound != @{$query}) {
			my $full = '';
			for my $j (0..$#sound) {
				if($j-$i >= 0 && $j-$i < @{$query}) {
					$full .= $query->[$j-$i]{full};
				} else {
					$full .= $sound[$j]{main} if $sound[$j]{main}; # TODO ???
				}
			}
			$query->[0]{clusters}{$full} = 1;
			
			if($is_vowel) {
				my $diphthong = join '', map {$_->{full}} @{$query};
				my $is_diphthong = 0;
				for my $j (reverse 0..$i-1) {
					last unless $sound[$j]{vowel};
					$diphthong = $sound[$j]{main} . $diphthong;
					$is_diphthong = 1;
				}
				for my $j ($i+@$query..$#sound) {
					last unless $sound[$j]{vowel};
					$diphthong .= $sound[$j]{main};
					$is_diphthong = 1;
				}
				if($is_diphthong) {
					$query->[0]{diphthongs}{$diphthong} = 1;
				}
			}
		}
	}
	return $any_match;
}

my @MOAs = ('Nasal','Plosive','Ejective plosive','Voiced plosive','Implosive plosive','Unreleased','Prestopped','Fricative','Ejective fricative','Voiced fricative','Affricate','Ejective affricate','Voiced affricate','Approximant','Flap','Trill','Lateral fricative','Lateral affricate','Lateral approximant','Lateral flap','Click');
my @plain_POAs = ('Bilabial','Labiodental','Labial-velar','Linguolabial','Dental','Alveolar','Postalveolar','Retroflex','Alveolo-palatal','Palatal','Palatal-velar','Velar','Uvular','Pharyngeal','Epiglottal','Glottal');
my @POAs;
for my $p (@plain_POAs) {
	push @POAs, "$p", "$p (ʲ)", "$p (ʷ)", "$p (ʲʷ)";
}
my %MOAs = map {lc $_ => 1} @MOAs;
my @HEIGHTS = ('Close','Near-close','Close-mid','Mid','Open-mid','Near-open','Open');
my @BACKNESSES = ('Front','Near-front','Central','Near-back','Back');
my %AFFRICATES = ('bilabial,labiodental'=>1, 'alveolar,dental'=>1, 'alveolar,postalveolar'=>1, 'alveolar,alveolo-palatal'=>1, 'alveolar,palatal-velar'=>1);
# 'alveolar,retroflex'=>1, 'alveolar,palatal'=>1, FIXME
my $ALTERNATE_W = {"sampa"=>"w","ipa"=>"w","wp"=>"Labial-velar_approximant","desc"=>"labial-velar approximant","type"=>"consonant","rounding"=>"unrounded","poa"=>"labial-velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiced"};
my $ALTERNATE_WH = {"sampa"=>"W","ipa"=>"ʍ","wp"=>"Voiceless_labial-velar_fricative","desc"=>"voiceless labial-velar fricative","type"=>"consonant","rounding"=>"unrounded","poa"=>"labial-velar","lateral"=>false,"moa"=>"approximant","implosive"=>false,"voicing"=>"voiceless"};

sub add_sounds_to_table {
	my ($table, @sounds) = @_;
	for my $s (@sounds) {
		$s = from_html($s);
		$s = normalize_sound($s);
		my $prev = undef; my $prev_info = undef;
		for my $c (separate_sounds($s)) {
			#warn "empty: $s" unless $c->{main};
			next unless $c->{main};
			my $info = $IPA{$c->{main}};
			my %info = %{$info//{}};
			while($c->{mod} =~ /(\[[^\]]*\]|[͜͡]?.)/g) {
				my $m = $1;
				$m =~ s{ɡ}{g}g;
				my $minfo = $IPA{$m};
				$info{$minfo->{menu}} = $minfo->{value} if $minfo && $minfo->{menu} && !$table->{is_plain};
				if($m =~ /^\[/) {
					$table->{brackets}{$m} = 1;
				} else {
					$table->{mods}{$m} = 1;
				}
			}
			if(!$info) {
				$table->{other}{$c->{main}} = 1;
				$prev = $prev_info = undef;
				next;
			}
			$c = {pre=>'', main=>$c->{main}, mod=>'', full=>$c->{main}} if $table->{is_plain};
			if($info->{type} eq 'consonant') {
				if($table->{is_plain} && $c->{main} eq "w") {
					%info = %$ALTERNATE_W;
				} elsif($table->{is_plain} && $c->{main} eq "ʍ") {
					%info = %$ALTERNATE_WH;
				}
				my $moa = $info{moa};
				if($c->{pre} =~ /[ᵐⁿᵑ]/) {
					$moa = "prenasalized $moa" if $MOAs{"prenasalized $moa"};
				}
				if($info{lateral}) {
					$moa = "lateral $moa" if $MOAs{"lateral $moa"};
				}
				if($info{ejective}) {
					$moa = "$info{ejective} $moa" if $MOAs{"ejective $moa"};
				}
				if($info{implosive}) {
					$moa = "implosive $moa" if $MOAs{"implosive $moa"};
				}
				if($info{voicing} eq 'voiced' && !$table->{is_plain}) {
					$moa = "voiced $moa" if $MOAs{"voiced $moa"};
				}
				my ($_j, $_w) = (ueq($info{"back-coarticulation"}, "palatalized"), ueq($info{"rounding"}, "rounded"));
				my $poa = $info{poa};
				if($_j && $_w) {
					$poa .= ' (ʲʷ)';
				} elsif($_j) {
					$poa .= ' (ʲ)';
				} elsif($_w) {
					$poa .= ' (ʷ)';
				}
				my $voicing = $info{voicing};
				$table->{consonants}{$moa}{$poa}{$voicing}{$c->{full}} = 1;
				$table->{poa}{$poa} = 1;
				if($prev && $prev_info->{type} eq "consonant") {
					if($prev_info->{moa} eq "plosive" &&
					   $info->{moa} eq "fricative" &&
					   ($info->{poa} eq $prev_info->{poa} ||
					    $AFFRICATES{$prev_info->{poa}.','.$info{poa}}) &&
					   $info->{voicing} eq $prev_info->{voicing} &&
					   $c->{main} ne 'h') {
						$table->{consonants}{$info{lateral} ? "lateral affricate" : "affricate"}{$poa}{$voicing}{$prev->{full}.$c->{full}} = 1;
					} elsif($prev_info->{moa} eq $info->{moa} &&
					        $info->{poa} eq 'bilabial' &&
					        $prev_info->{poa} eq 'velar' &&
					        $prev_info->{voicing} eq $info->{voicing}) {
						$table->{consonants}{$info->{moa}}{"labial-velar"}{$voicing}{$prev->{full}.$c->{full}} = 1;
						$table->{poa}{"labial-velar"} = 1;
					}
				}
			}
			if($info->{type} eq 'vowel') {
				my $height = $info{height};
				my $backness = $info{backness};
				my $rounding = $info{rounding};
				$table->{vowels}{$height}{$backness}{$rounding}{$c->{full}} = 1;
				$table->{backness}{$backness} = 1;
			}
			$prev = $c; $prev_info = $info;
		}
	}
}

sub mkeys {
	if(defined($_[0])) {
		return keys %{$_[0]};
	}
	return ();
}

sub circle_diacritics { (local $_) = @_;
	s/^([\x{300}-\x{36f}])/◌$1/;
	s/([͜͡])$/$1◌/;
	s/([⁰¹²³⁴⁵₀₁₂₃₄₅])/$SUP_SUB{$1}/g;
	return $_;
}

sub sort_key { (local $_) = @_;
	if($_ eq '¹') {return "\x{2071}";}
	if($_ eq '²') {return "\x{2072}";}
	if($_ eq '³') {return "\x{2073}";}
	s{\[([+-])(.*)}{[$2$1};
	return $_;
}

sub make_sounds_table {
	my ($table, $url) = @_;
	$url //= $SEARCH_URL . '?q=';
	if($table->{consonants}) {
		print qq{<table border>\n};
		my @poas;
		for my $poa (@POAs) {
			next unless $table->{poa}{lc $poa};
			push @poas, lc $poa;
		}
		for my $moa (@MOAs) {
			next unless $table->{consonants}{lc $moa};
			$moa = lc $moa;
			print qq{<tr>\n};
			for my $poa (@poas) {
				print qq{<td>};
				my $t = $table->{consonants}{$moa}{$poa};
				my @sounds = (
					(sort {$a cmp $b} mkeys $t->{voiceless}),
					(sort {$a cmp $b} mkeys $t->{voiced}),
					(sort {$a cmp $b} mkeys $t->{creaky}),
					(sort {$a cmp $b} mkeys $t->{breathy})
				);
				#print mkeys($t)," $poa $moa";
				for my $s (@sounds) {
					my $e = escape($s);
					print qq{<a href="$url$e">$s</a> };
				}
				print "\n";
			}
		}
		print "</table>\n";
	}
	if($table->{vowels}) {
		print qq{<table border>\n};
		my @backnesses;
		for my $bk (@BACKNESSES) {
			next unless $table->{backness}{lc $bk};
			push @backnesses, lc $bk;
		}
		for my $h (@HEIGHTS) {
			next unless $table->{vowels}{lc $h};
			$h = lc $h;
			print qq{<tr>\n};
			for my $bk (@backnesses) {
				print qq{<td>};
				my $t = $table->{vowels}{$h}{$bk};
				my @sounds = (
					(sort {$a cmp $b} mkeys $t->{unrounded}),
					(sort {$a cmp $b} mkeys $t->{less}),
					(sort {$a cmp $b} mkeys $t->{rounded}),
					(sort {$a cmp $b} mkeys $t->{more})
				);
				for my $s (@sounds) {
					my $e = escape $s;
					print qq{<a href="$url$e">$s</a> };
				}
				print "\n";
			}
		}
		print "</table>\n";
	}
	if($table->{mods}) {
		print qq{<p>};
		my @sounds = sort {sort_key($a) cmp sort_key($b)} keys %{$table->{mods}};
		for my $m (@sounds) {
			my $e = escape($m);
			my $c = circle_diacritics($m);
			print qq{<a href="$url$e">$c</a> };
		}
		print qq{</p>\n};
	}
	if($table->{other}) {
		print qq{<p>};
		my @sounds = sort {$a cmp $b} keys %{$table->{other}};
		unshift @sounds, '_#' if $url eq $SEARCH_URL . '?q=';
		unshift @sounds, '#_' if $url eq $SEARCH_URL . '?q=';
		for my $m (@sounds) {
			next if $m =~ /^[()*,\-\[\]_?;\/{}~](?!#)/;
			my $e = escape($m);
			my $c = circle_diacritics($m);
			print qq{<a href="$url$e">$c</a> };
		}
		print qq{</p>\n};
	}
	if($table->{brackets}) {
		print qq{<p>};
		my @sounds = sort {sort_key($a) cmp sort_key($b)} keys %{$table->{brackets}};
		for my $m (@sounds) {
			my $e = escape($m);
			my $c = circle_diacritics($m);
			print qq{<a href="$url$e">$c</a> };
		}
		print qq{</p>\n};
	}
}

our $ALL = "all.html";

sub print_table {
	my @table = @_;
	my ($main_heading, $section_heading) = ({sid=>''}, {sid=>''});
	for my $item (@table) {
		if($item->{context}{main_sid} ne $main_heading->{sid}) {
			$main_heading = $item->{main_heading} = {sid=>$item->{context}{main_sid}, name=>escapeHTML($item->{context}{main_name}), rows=>1};
		} else {
			$main_heading->{rows}++;
		}
		if($item->{context}{sid} ne $section_heading->{sid}) {
			$section_heading = $item->{section_heading} = {sid=>$item->{context}{sid}, name=>escapeHTML($item->{context}{name}), rows=>1};
		} else {
			$section_heading->{rows}++;
		}
	}
	for my $item (@table) {
		print '<tr>';
		if(defined($item->{main_heading})) {
			print qq{<td rowspan=$item->{main_heading}{rows}><a href="$ALL#$item->{main_heading}{sid}">$item->{main_heading}{name}</a>};
		}
		if(defined($item->{section_heading})) {
			print qq{<td rowspan=$item->{section_heading}{rows}><a href="$ALL#$item->{section_heading}{sid}">$item->{section_heading}{name}</a>};
		}
		print qq{<td><a href="$ALL#$item->{id}">$item->{change}</a>\n};
	}
}

sub search_index {
	my $query = $_[0];
	my $chars = join '', map {$_->{full}} (@{$query->{from}}, @{$query->{to}});
	#print STDERR Dumper($query);
	#print STDERR $chars, "\n";
	return undef unless $INDEX_FILE;
	
	open my $fh, '<', $INDEX_FILE or die "Can't open index file";
	binmode $fh;
	local $/ = \$INT_SIZE;
	my $size = unpack $INT_PATTERN, <$fh>;
	my @stat = stat $DATA_FILE;
	return undef if $size != $stat[7];
	while(1) {
		last if eof;
		my $char = chr unpack $INT_PATTERN, <$fh>;
		my $length = unpack $INT_PATTERN, <$fh>;
		if(index($chars, $char) < 0) {
			seek $fh, $length*$INT_SIZE, 1;
			next;
		}
		#print STDERR "$chars $char", "\n";
		my @pos;
		for my $i (1..$length) {push @pos, unpack $INT_PATTERN, <$fh>;}
		return [@pos];
	}
	return undef;
}

sub plural($$$) {
	my ($num, $sing, $plur) = @_;
	if($num == 1) {
		return "$num $sing";
	} else {
		return "$num $plur";
	}
}

sub search_html {
	my $query = $_[0];
	my (@from_table, @to_table, @context_table);
	my $sound_desc = $SOUND_DESC{$query->{raw}};
	read_data(
		change => sub {
			my ($line, $id, %context) = @_;
			my %change = parse_sound_change($line);
			my $match = match_change($query, %change);
			$line =~ s{(‹|^)[^‹›]+›}{}g;
			$line =~ s{[«»]}{}g;
			if($match =~ /t/) {
				push @to_table, {context=>{%context}, change=>$line, id=>$id};
			}
			if($match =~ /f/) {
				push @from_table, {context=>{%context}, change=>$line, id=>$id};
			}
			if($match =~ /c/) {
				push @context_table, {context=>{%context}, change=>$line, id=>$id};
			}
		},
		pchange => sub {
			my ($pline, %context) = @_;
			my %change = parse_sound_change_processed($pline);
			my ($line, $id) = @change{'html', 'id'};
			$id = $context{sid} . '-' . $id;
			my $match = match_change($query, %change);
			$line =~ s{(‹|^)[^‹›]+›}{}g;
			$line =~ s{[«»]}{}g;
			if($match =~ /t/) {
				push @to_table, {context=>{%context}, change=>$line, id=>$id};
			}
			if($match =~ /f/) {
				push @from_table, {context=>{%context}, change=>$line, id=>$id};
			}
			if($match =~ /c/) {
				push @context_table, {context=>{%context}, change=>$line, id=>$id};
			}
		},
		seeks => search_index($query)
	);
	print qq{<section>\n};
	if(!!@from_table + !!@to_table + !!@context_table > 1) {
		print qq{<p>};
		if(@from_table) {
			print qq{<a href="#from">}, plural(scalar(@from_table), 'result', 'results'), qq{ }, format_query('forward', $query), qq{</a>   };
		}
		if(@to_table) {
			print qq{<a href="#to">}, plural(scalar(@to_table), 'result', 'results'), qq{ }, format_query('reverse', $query), qq{</a>   }
		}
		if(@context_table) {
			print qq{<a href="#context">}, plural(scalar(@context_table), 'result', 'results'), qq{ }, format_query('context', $query), qq{</a>   }
		}
		print qq{</p>\n\n};
	}
	if(@{$query->{from}} > 0 && @{$query->{to}} == 0 && $query->{from}[0]{variants} && keys %{$query->{from}[0]{variants}} > 1) {
		print qq{<p style="font-size:1.2em">};
		my $plain = join '', map {$_->{full}} @{$query->{from}};
		for my $v (sort {$a cmp $b} keys %{$query->{from}[0]{variants}}) {
			my $e = escape($v);
			if($v eq $plain) {
				print qq{<a href="$SEARCH_URL?q=plain+$e">plain</a> };
			} else {
				my $c = circle_diacritics($v);
				print qq{<a href="$SEARCH_URL?q=$e">$c</a> };
			}
		}
		print qq{</p>};
	}
	#if(@{$query->{from}} > 0 && @{$query->{to}} == 0 && $query->{from}[0]{clusters} && keys %{$query->{from}[0]{clusters}} > 1) {
	#	print qq{<p style="font-size:1.2em">};
	#	for my $v (sort {$a cmp $b} keys %{$query->{from}[0]{clusters}}) {
	#		my $e = escape($v);
	#		my $c = circle_diacritics($v);
	#		print qq{<a href="$SEARCH_URL?q=$e">$c</a> };
	#	}
	#	print qq{</p>};
	#}
	if(@{$query->{from}} > 0 && @{$query->{to}} == 0 && $query->{from}[0]{diphthongs} && keys %{$query->{from}[0]{diphthongs}} >= 1) {
		print qq{<p style="font-size:1.2em">};
		for my $v (sort {$a cmp $b} keys %{$query->{from}[0]{diphthongs}}) {
			my $e = escape($v);
			my $c = circle_diacritics($v);
			print qq{<a href="$SEARCH_URL?q=$e">$c</a> };
		}
		print qq{</p>};
	}
	print qq{</section>};
	if(@from_table) {
		print qq{<section id="from">\n<h2>}, format_query('forward', $query), qq{</h2>\n};
		print '<p>', plural(scalar(@from_table), 'match', 'matches'), "\n<table border>\n";
		print_table(@from_table);
		print "</table>\n</section>\n\n";
	}
	if(@to_table) {
		print qq{<section id="to">\n<h2>}, format_query('reverse', $query), qq{</h2>\n};
		print '<p>', plural(scalar(@to_table), 'match', 'matches'), "\n<table border>\n";
		print_table(@to_table);
		print "</table>\n</section>\n\n";
	}
	if(@context_table) {
		print qq{<section id="context">\n<h2>}, format_query('context', $query), qq{</h2>\n};
		print '<p>', plural(scalar(@context_table), 'match', 'matches'), "\n<table border>\n";
		print_table(@context_table);
		print "</table>\n</section>\n\n";
	}
	if(!@from_table && !@to_table && !@context_table) {
		print "<section>";
		if($sound_desc) {
			print "<p>$sound_desc</p>\n";
		}
		print "<p>No matches</p></section>\n";
	}
}

1;
