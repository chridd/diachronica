common = Makefile html/Diachronica.pm

all: data-raw.txt html/all.html html/index-diachronica.pdf html/inline-plain.html html/full-table.html html/diachronica-data html/diachronica-index

# patch -o html/index-diachronica.tex Index.tex xetex-patch
# edit html/index-diachronica.tex
data-raw.txt: html/index-diachronica.tex tex2data.pl $(common)
	perl tex2data.pl
# patch -o data.txt data-raw.txt data-patch
# edit data.txt

html/all.html: data.txt generate-html.pl $(common)
	perl generate-html.pl > html/all.html
#html/data.txt: data.txt $(common)
#	grep -v '^T' data.txt > html/data.txt
html/index-diachronica.pdf: html/index-diachronica.tex
	latexmk -xelatex -halt-on-error html/index-diachronica.tex
	mv index-diachronica.pdf html/
html/inline-plain.html: data.txt plain-table.pl $(common)
	perl plain-table.pl > html/inline-plain.html
html/full-table.html: data.txt full-table.pl $(common)
	perl full-table.pl > html/full-table.html
html/diachronica-data: data.txt preprocess-data.pl $(common)
	perl preprocess-data.pl > html/diachronica-data
html/diachronica-index: html/diachronica-data make-index.pl $(common)
	perl make-index.pl html/diachronica-data > html/diachronica-index

xetex-patch: Index.tex html/index-diachronica.tex $(common)
	diff Index.tex html/index-diachronica.tex > xetex-patch; true
data-patch: data.txt data-raw.txt $(common)
	diff data-raw.txt data.txt > data-patch; true
