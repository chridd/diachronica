#! /usr/bin/perl -Ihtml/
use strict; use utf8; use warnings;
use Diachronica;

my $html = make_html_of_sections();
binmode STDOUT, ':utf8';
print qq{<!doctype html>
<meta charset=\"utf-8\">
<link rel="icon" href="icon.png">
<link rel="apple-touch-icon" href="icon.png">
$Diachronica::STYLESHEET<title>Searchable Index Diachronica: All sections (10.2)</title>
<h1>Searchable Index Diachronica: All sections (10.2)</h1>
<nav class=bc><p><a href="index.shtml">Searchable Index Diachronica</a> » <strong>All sections</strong></p></nav>
<p><small>(Note that I did <em>not</em> make the Index, I only converted it to HTML and made the <a href="index.html">search tool</a>.)</small></p>
};
#<title>Index Diachronica</title>
print '<section><nav class=toc>
<h2>Contents</h2>
', make_toc(0, '', make_html()), '
</nav></section>
';
print $html;
