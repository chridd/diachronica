#! /usr/bin/perl -Ihtml/
use utf8; use strict; use warnings;
use Diachronica;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

my $table = {};

read_data(
	change => sub {
		my %c = parse_sound_change($_[0]);
		add_sounds_to_table($table, sounds(%c));
	}
);
print qq{<!doctype html>
<meta charset="utf-8">
<link rel="icon" href="icon.png">
<link rel="apple-touch-icon" href="icon.png">
$Diachronica::STYLESHEET<title>Searchable Index Diachronica: Full sounds table</title>
<style>#searchtable a {text-decoration:none; font-size:1.2em;} /* better able to see diacritics */</style>

<h1>Searchable Index Diachronica: Full sounds table</h1>
<nav class=bc><p><a href="index.shtml">Searchable Index Diachronica</a> » <strong>Full table</strong></p></nav>

<section>
<h2>Search for a sound</h2>
<p>Click a sound below to see all sound changes to and from that sound.  (Other mentions of a sound are not included.)
<div id="searchtable">
};
make_sounds_table($table);
print qq{</div>\n</section>\n};
