#! /usr/bin/perl -Ihtml/
use Diachronica;
use Encode;
binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

my $query = parse_query(decode('utf-8', $ARGV[0]));
my $fquery = format_query('either', $query);
print qq{<meta charset="utf-8">\n<title>Index Diachronica: $fquery</title>\n};
search_html($query);
