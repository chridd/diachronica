# for debugging/testing

open my $fh, '<', $ARGV[0];
binmode $fh;
binmode STDOUT, ':utf8';
local $/ = \4;
my $INT_PATTERN = 'V';
print scalar(unpack $INT_PATTERN, <$fh>), "\n";
while(1) {
	last if eof;
	print "Char: ", (chr unpack $INT_PATTERN, <$fh>), "\n";
	my $length = unpack $INT_PATTERN, <$fh>;
	print "\tLength: ", ($length), "\n";
	for my $i (1..$length) {print "\t", (unpack $INT_PATTERN, <$fh>), "\n";}
}

