#! /usr/bin/perl -Ihtml/
use utf8; use warnings; use strict;
use Diachronica;
binmode STDOUT, ':utf8';

read_data(
	section => sub {
		my %context = @_;
		print "s $context{sid}\cA$context{name}\n";
	},
	change => sub {
		my ($line, $id, %context) = @_;
		$id = substr $id, length($context{sid}) + 1;
		my $html = $line;
		$html =~ s{(‹|^)[^‹›]+›}{}g;
		$html =~ s{[«»]}{}g;
		
		my %change = parse_sound_change($line);
		print "c $id\cA$html\cA",
			join("\cB", map {join "\cC", @$_} @{$change{from}}), "\cA",
			join("\cB", map {join "\cC", @$_} @{$change{to}}), "\cA",
			join("\cB", map {join "\cC", @$_} @{$change{context}}), "\n";
	}
);
