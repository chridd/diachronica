Index Diachronica is a list of sound changes from many, many, many, *many* different languages. However, it is large, and it doesn't use Unicode, making it hard to search for specific sounds. Therefore, I made a Unicode PDF, and also an HTML version (since I know at least Apple's Preview has trouble searching for certain Unicode characters (like ʷ) even if a document is in Unicode), and also made a search tool. (Note that I did not make the Index, I only converted it to HTML and made the search tool.) 

The searching feature can currently be used online [on my website](https://chridd.nfshost.com/diachronica/).  The code for it and various files are in this repository; there's still some stuff I should probably do to make this repository more usable, but this is what I have for now.

Viewing
=======

If you just want to view the index, the following files may be helpful:

* `html/index-diachronica.pdf`: A PDF version of the Index in Unicode
* `html/all.html`: An HTML version of the Index in Unicode

You can also search the Index from the command line if you have Perl and a terminal that supports Unicode input and output.  ([This](https://stackoverflow.com/questions/388490/unicode-characters-in-windows-command-line-how) suggests `chcp 65001` on Windows, but I don't have Windows so I can't test that.  TODO: Add X-SAMPA support for people whose consoles don't support Unicode.)  Change directories to whatever directory you put the files in and type something like

	perl search.pl ɬ

Data files
==========

I started with `Index.tex`, which was the LaTeX file for the original Index Diachronica (which I didn't make).  I manually made some changes to make it compatible with XeLaTeX so that I could make a Unicode PDF; the XeLaTeX file is in `html/index-diachronica.tex`, and is also linked at the top of the online version.  Both of these files are intended to be human-readable (after using LaTeX to convert them to PDF).  I then converted them into more machine-readable formats; however, since I'm not aware of any standard format for things like this, I made my own format.

`data.txt` is partially-processed, mostly to find which lines have sound changes, and I've manually made some edits whenever I noticed the program having trouble interpreting a change.  (`data-raw.txt` is the same thing, but without those manual changes.)  It's a text file, encoded as UTF-8, where each line starts with a capital letter (followed by a space) indicating the type of line:

* `#`: Indicates a comment (ignored by the program)
* `S`: Indicates the start of a new section.  Lines are of the form `S 1.2.3 Section Title`, where `1.2.3` is the section number, subsection number, subsubsection number, etc.
* `T`: Indicates some text, in HTML format, which is included in `html/all.html` but otherwise ignored.
* `<`, `R`, `>`: Indicates a table, usually (but not always) for protolang sound inventories.  `<` on a line by itself indicates the start of the table, `>` on a line by itself indicates the end of the table, and each line starting with `R` is a row.  Columns are separated by tab characters.
* `C`: Indicates a sound change, in HTML, as it will be displayed to the user; this is also parsed by the program.  `«...»` surrounds parts of the line that are included when displaying the sound change to the user but ignored by the program while figuring out what sounds are involved.  `‹...›` surrounds parts of the line that the program will see when figuring out what sounds are involved, but which aren't displayed to the user.  (If there is no closing `»` or `›`, then it's implied at the end of the line.)

`html/diachronica-data` is more processed.  Its format is similar to `data.txt`, but lines start with lowercase letters.  Each line is separated into fields using ASCII characters with codes 1, 2, and 3.  Depending on your text editor, these may appear invisible, as boxes, or as `^A`, `^B`, and `^C`.  In many programming languages, these characters can be entered as `\x01`, `\x02`, `\x03`, or as `\u0001`, `\u0002`, `\u0003`.

* `s`: Indicates the start of a new section.  Each line has two fields, separated by character 1 (`^A`).  The first field is the ID for the section (the part after the `#` in the URL for the section); the second field is the section number and section title, in the same format as `data.txt`.
* `c`: Indicates a sound change.  Each line has five fields, separated by character 1 (`^A`):
    1. The ID for the sound change.  If a sound change is in the section with ID `Classical-Arabic`, and the sound change's ID is, `m`, then the URL `https://chridd.nfshost.com/diachronica/all#Classical-Arabic-m` will directly link to that change in context.
    2. The text of the sound change, in HTML, as displayed to the user.
    3. Any sounds in the "from" part of the change (before the arrow).
    4. Any sounds in the "to" part of the change (after the arrow).
    5. Any sounds in the "context" part of the change (after the slash).

In `c` lines, if there are multiple sound changes on the same line, where each "from" sound corresponds to a different "to" sound, then within fields 3 and 4, those changes will be separated by character 2.  If there are multiple alternatives within a single change, where multiple "from" sounds correspond to different "to" sounds or vice versa, then within fields 3, 4, and 5, those alternatives will be separated by character 3.  For instance, in the following change:

> [z dz ɡ → ɡ {z,dz} ɡ(ʷ)](https://chridd.nfshost.com/diachronica/all#Awngi-z-dz-%C9%A1)

"z", "dz", and "g" are considered separate changes, since each of them correspond to a different result, so they're separated by character 2.  "z" and "dz" are considered different alternatives, since they both come from "dz", so they're separated by character 3.  Thus, the line for this sound change looks like this:

> `c z-dz-ɡ<CHAR 1>z dz ɡ → ɡ {z,dz} ɡ(ʷ)<CHAR 1>z<CHAR 2>dz<CHAR 2>ɡ<CHAR 1>ɡ<CHAR 2>z<CHAR 3>dz<CHAR 2>ɡ<CHAR 3>ɡʷ<CHAR 1>`

Within the "context" field (field 5), character 2 has a different meaning; it refers to negated contexts.  For instance, in the following change:

> [w → ∅ / \_# ! k(ː)\_](https://chridd.nfshost.com/diachronica/all#Kennebec-River-Abenaki-w)

Field 5 looks like `_#<CHAR 2>k_<CHAR 3>kː_`.  (Note also that the parentheses are interpreted as two alternatives.)

(TODO also add the CSV file I made for [this comment](https://chridd.nfshost.com/diachronica/comments#130) and document its format.)

Mirroring/Hosting
=================

(TODO test this to make sure everything works as expected)

If you want to host your own copy of the search tool, everything you need should be in the `html` directory.  This is a Perl CGI script, so you will need Perl installed and to have a server that lets you run CGI scripts.  Also, I have not tested this on Windows.  I've included a `.htaccess` file for Apache, which may or may not be necessary depending on your configuration; if you're using a different server, then you can probably get it to work but I personally don't know much about other server software.

Some configuration options:

* If Perl is not in `/usr/bin/perl`, then you'll need to change the first line of `search.cgi` (the part after the `#!` but before the `-TI`).
* If you want to apply a stylesheet to the search page, in `search.cgi` uncomment the line starting with `#$Diachronica::STYLESHEET` and replace the path with the path to your stylesheet.  The HTML files can be edited manually (inline-plain.html should not include the stylesheet).

Also, as written, this requires server-side includes (.shtml).  If you want, you can instead copy the contents of `inline-plain.html` into `index.shtml`, and then you won't need server-side includes.

Also you may need to make sure that `search.cgi` has execute permissions.

Editing
=======

TODO

Licensing
=========

I don't own the actual text of the Index, and I don't claim copyright on the modified versions I made.  See the text of that file for copyright information.

I should decide on a license for the code, though.  (TODO)
