#! /usr/bin/perl -Ihtml/
use utf8; use warnings; use strict;
use Diachronica;

binmode STDERR, ':utf8';

$Diachronica::DATA_FILE = $ARGV[0] if @ARGV;

my $output = {};

read_data(
	main_section => sub {
		my (%context) = @_;
		$output->{main_section} = $output->{section} = $context{byte};
	},
	subsection => sub {
		my (%context) = @_;
		$output->{section} = $context{byte};
	},
	change => sub {
		my ($change, $id, %context) = @_;
		my %chars;
		my %change = parse_sound_change($change);
		for my $s (sounds %change) {
			#$s =~ s{\[[^\]]+\]}{}g;
			$s =~ s{<[^>]+>}{}g;
			$s =~ s{&amp;}{&}g;
			$s = normalize_sound($s);
			%chars = (%chars, map {$_,1} split //, $s);
		}
		my $o = {main_section=>$output->{main_section}, section=>$output->{section}, change=>$context{byte}};
		for my $ch (keys %chars) {
			push @{$output->{$ch}}, $o;
		}
	},
	pchange => sub {
		my ($pline, %context) = @_;
		my %chars;
		my %change = parse_sound_change_processed($pline);
		for my $s (sounds %change) {
			#$s =~ s{\[[^\]]+\]}{}g;
			$s =~ s{<[^>]+>}{}g;
			$s =~ s{&amp;}{&}g;
			$s = normalize_sound($s);
			%chars = (%chars, map {$_,1} split //, $s);
		}
		my $o = {main_section=>$output->{main_section}, section=>$output->{section}, change=>$context{byte}};
		for my $ch (keys %chars) {
			push @{$output->{$ch}}, $o;
		}
	}
);

#print STDERR keys %{$output}, "\n";
delete $output->{main_section};
delete $output->{section};

my $INT_SIZE = 4;
my $INT_PATTERN = 'V';

binmode STDOUT;

my @stat = stat $Diachronica::DATA_FILE;
print pack $INT_PATTERN, $stat[7]; # size

for my $c (sort {@{$output->{$a}} <=> @{$output->{$b}} || $a cmp $b} keys %{$output}) {
	warn "length $c != 1" unless length($c) == 1;
	next if length($c) != 1;
	my @offsets;
	my $main_sec = -1;
	my $sec = -1;
	for my $s (@{$output->{$c}}) {
		if($s->{main_section} != $main_sec) {
			$main_sec = $sec = $s->{main_section};
			push @offsets, $main_sec;
		}
		if($s->{section} != $sec) {
			$sec = $s->{section};
			push @offsets, $sec;
		}
		push @offsets, $s->{change};
	}
	print map {pack $INT_PATTERN,$_} ord $c, scalar(@offsets), @offsets;
	print STDERR $c;
}
print STDERR "\n";
